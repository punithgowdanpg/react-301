import React from 'react';
import { shallow } from 'enzyme';
import Profile from './Profile';
describe('User Profile component', () => {
    test('should render Profile correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<Profile />);
        expect(wrapper).toMatchSnapshot();
    });
});