import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { styled } from "@material-ui/styles";
import Button from '@material-ui/core/Button';
import history from '../../shared/utils/history';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';


const styles = theme => ({   
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  card: {
    margin: 'auto',
    maxWidth: 400,
  },
  media: {
    height: 0,
    paddingTop: '100%', // 16:9
  },
  actions: {
    display: 'flex',
  },
  image: {
        width: 140,
        height: 140,
        
  },
  img: {
    margin: 'auto',
    display: 'block',
    maxWidth: '100%',
    maxHeight: '100%',
  },
});
const FavouritesButton = styled(Button)({
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    // background: 'linear-gradient(45deg, #1a1c23 30%, #cc2929 90%)',
    // boxShadow: '0 3px 5px 2px rgb(104, 64, 69)',
    border: 0,
    borderRadius: 3,
    color: "white",
    height: 48,
    padding: "0 30px"
  });
 
  const HistoryButton = styled(Button)({
    background: "linear-gradient(45deg, #885b8e 30%, #21CBF3 90%)",
    boxShadow: "0 3px 5px 2px rgba(33, 203, 243, .3)",
    // background: 'linear-gradient(45deg, #1a1c23 30%, #cc2929 90%)',
    // boxShadow: '0 3px 5px 2px rgb(104, 64, 69)',
    border: 0,
    borderRadius: 3,
    color: "white",
    height: 48,
    padding: "0 30px"
  });
class Profile extends React.Component {
  handleFavourites = () => {
    history.push(`/favourites/`);
};
  handleHistory = () => {
    history.push(`/userhistory/`);
  };
  handleClick = () => {
    history.push('/categories');
  };
  render(){
  const { classes } = this.props;
  return (
   
    <div className={classes.root}>
       
      <Card className={classes.card}>
      
      <CardMedia
        className={classes.media}
        image={this.props.auth.userImg+'?sz=500'}
        title="User Image"
      />
      <CardContent>
      <Typography gutterBottom>User Name: {this.props.auth.userName}</Typography>
                <Typography gutterBottom>Email: {this.props.auth.userEmail} </Typography>
      </CardContent>
      <CardActions className={classes.actions} disableActionSpacing>
      <FavouritesButton variant="contained" color="secondary" className={classes.button} onClick = {this.handleFavourites }>
     Favourites
    </FavouritesButton> 
    <HistoryButton variant="contained" color="secondary" className={classes.button} onClick = { this.handleHistory }>
      History
    </HistoryButton> 
          </CardActions>
    </Card>
    </div>
  );
}
}


Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Profile);

