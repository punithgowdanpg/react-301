import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Rating from 'react-rating';
import Fab from '@material-ui/core/Fab';
import FavoriteIcon from '@material-ui/icons/Favorite';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import TextField from '@material-ui/core/TextField';
import ReplyIcon from '@material-ui/icons/Reply';
import moment from 'moment';
import emptyStar from '../../../assets/images/star-empty.png';
import fullStar from '../../../assets/images/star-full.png';
import SendGift from '../containers/SendGift';
import Loader from '../../shared/components/Loader/Loader';
import history from '../../shared/utils/history';
import { withSnackbar } from 'notistack';


const styles = theme => ({
    root: {
        flexGrow: 1,
        [theme.breakpoints.down('sm')]: {
            padding: '20px'
        }
    },
    productContainer: {
        marginTop: '10px',
    },
    imgContainer: {
        width: '500px',
        border: '1px solid gray',
        textAlign: 'center',
        marginLeft: '6%',
        padding: '10px',
        borderRadius: '25px',
        maxHeight: '520px',
        overflow: 'hidden',
        [theme.breakpoints.down('sm')]: {
            width: '320px',
            maxHeight: '180px',
            marginLeft: '10px',
            padding: '0px'
        },
    },
    productImg: {
        transition: 'transform .5s ease',
        width: '100%',
        "&:hover": {
            transform: 'scale(1.2)',
            cursor: 'pointer',
        }
    },
    productInfo: {
        marginTop: '10px',
        textAlign: 'left'
    },
    buyoutPoints: {
        fontSize: '18px',
        fontWeight: 'bolder',
    },
    ratingWrap: {
        marginTop: '20px',
        '& img': {
            width: '28px'
        }
    },
    productName: {
        color: '#212121',
        display: 'contents',
        letterSpacing: '.1em',
        [theme.breakpoints.down('sm')]: {
            fontSize: '20px'
        }
    },
    description: {
        padding: '30px 10px',
        fontStyle: 'Roboto,Arial,sans-serif'
    },
    descLabel: {
        fontWeight: 'bold',
        marginTop: '20px'
    },
    buyNow: {
        marginTop: '25px',
        textAlign: 'center'
    },
    addFavourite: {
        marginLeft: '80px',
        background: '#cc2929',
        [theme.breakpoints.down('sm')]: {
            marginLeft: '20px',
        }
    },
    ratingStar: {
        display: 'block',
        border: '3px solid #cc2929',
        background: '#fff',
        borderRadius: '50px',
        width: '175px',
        textAlign: 'center'
    },
    addCommentBtn: {
        margin: '10px 20px'
    }
});
class ProductDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: '',
            rating: 0,
            isFavourite: false
        }
    }
    componentDidMount() {
        const productId = this.props.match.params.productId;
        const { userId } = this.props.auth;
        
        this.props.getProductDetail(productId);
        this.props.getProductComments(productId);
        if(userId){
            this.props.getFavourites(userId);
        }
    }
    updateRating = (rating) => {
        const { userName } = this.props.auth;
        const reviewedAt = moment();
        const productId = this.props.match.params.productId;
        this.setState({ rating })
        this.props.updateRating(productId, userName, rating, reviewedAt);
    }
    updateComment = (e) => {
        this.setState({
            comment: e.target.value
        });
    }
    addFavourite = (e) => {
        const { name, imageUrl, buyoutPoints, id } = this.props.productDetail;
        const addedAt = moment();
        const { userId } = this.props.auth;
        this.props.addFavorites(name, imageUrl, buyoutPoints, id, addedAt, userId);
        this.props.enqueueSnackbar('Added to favorites ', { variant: 'success' })
    }
    removeFavourite = (e) => {
        const { id } = this.props.productDetail;

        this.props.removeFavorites(id);
        this.props.enqueueSnackbar('Removed from favorites ', { variant: 'success' })
    }
    addComments = () => {
        const commentAt = moment();
        const comment = this.state.comment;
        const productId = this.props.match.params.productId;
        this.props.updateComment(productId, comment, commentAt);
        this.setState({ comment: '' });
    }
    static getDerivedStateFromProps(nextProps, prevState){
        const { favourites } = nextProps;
        const productId = nextProps.productDetail.id;
        if(nextProps.favourites !== prevState.favourites) {
            if (favourites.filter(e => e.id === productId).length > 0) {
                return { isFavourite: true }
            }else {
                return { isFavourite: false }
            }
        }
    }    
    handleCategories = () => {
        history.push('/categories');
      };
      handleHome = () => {
        history.push('/');
      };
     
    render() {
        const { classes, auth, productComments } = this.props;
        const { name, brand, desc,
            imageUrl, buyoutPoints,
            rating, expiryDays
        } = this.props.productDetail;
        let currentRating;
        if(this.state.rating > 0){
            currentRating = this.state.rating;
        }else {
            currentRating = rating;
        }
        // sort productComments by Date
        productComments.sort(function (a, b) {
            return new Date(b.commentedAt) - new Date(a.commentedAt);
        });

        if (name === '') {
            return <Grid container className={classes.root} spacing={24} alignItems="center"
                justify="center" style={{ height: "90vh" }}> <Loader /> </Grid>
        }
        else return (
            <Grid className={classes.root} container justify="center">
                <Grid className={classes.productContainer} item xs={12} md={6}>
                    <div class={classes.imgContainer}>
                        <img className={classes.productImg} src={`${imageUrl}`} alt="product" />
                    </div>
                    <div className={classes.buyNow}>
                        <SendGift productId={this.props.match.params.productId}
                            name={name} brand={brand} buyoutPoints={buyoutPoints} expiryDays={expiryDays}
                        />
                        {this.state.isFavourite && 
                            <Fab disabled={!auth.isSignedIn} onClick={this.removeFavourite} className={classes.addFavourite} color="secondary" aria-label="Edit">
                                <FavoriteIcon />
                            </Fab>
                        }
                        {!this.state.isFavourite && 
                        <Fab disabled={!auth.isSignedIn} onClick={this.addFavourite} className={classes.addFavourite} aria-label="Edit">
                            <FavoriteIcon />
                        </Fab>
                        }
                    </div>
                </Grid>
                <Grid className={classes.productInfo} item xs={12} md={6}>
                    <Typography className={classes.productName} variant="h4" gutterBottom>
                        {name}
                    </Typography>
                    <div className={classes.description}>
                        <Typography variant="subheading" gutterBottom>
                            <span className={classes.descLabel} >Description:</span>{desc}
                        </Typography>
                        </div>
                       <Typography className={classes.buyoutPoints} variant="subtitle2" gutterBottom>
                           Brand: {brand}
                        </Typography> 
                    <Typography className={classes.buyoutPoints} variant="subtitle2" gutterBottom>
                    Points: {buyoutPoints}
                    </Typography>
                    <div className={classes.ratingWrap}>
                        <Typography variant="h6" gutterBottom>
                            Ratings:
                        </Typography>
                        <span className={classes.ratingStar}>
                            <Rating
                                onChange={this.updateRating}
                                initialRating={currentRating}
                                readonly={!auth.isSignedIn}
                                emptySymbol={<img alt="empty star" src={emptyStar} className="icon" />}
                                fullSymbol={<img alt="full star" src={fullStar} className="icon" />}
                            />
                         </span>
                    </div>
                    {auth.isSignedIn && (
                        <React.Fragment>
                            <Typography variant="h6" gutterBottom>
                                Reviews:
                            </Typography>
                            <TextField
                                id="outlined-full-width"
                                label="Your Comments"
                                placeholder="enter comment..."
                                helperText="your feedback is highly appreciated"
                                fullWidth
                                multiline={true}
                                rows={2}
                                rowsMax={3}
                                variant="outlined"
                                margin="normal"
                                value={this.state.comment}
                                onChange={this.updateComment}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                            <Fab className={classes.addCommentBtn} variant="extended" disabled={!this.state.comment} onClick={this.addComments} aria-label="Delete" >
                                <ReplyIcon className={classes.extendedIcon} />  Add Comments
                            </Fab>
                        </React.Fragment>
                    )}
                    <List className={classes.root}>
                        {productComments.map((comments) => {
                            return (
                                <ListItem key={comments.id}>
                                    <Avatar>
                                        <ImageIcon />
                                    </Avatar>
                                    <ListItemText primary={comments.userName} secondary={comments.comment} />
                                </ListItem>
                            );
                        })}
                    </List>
                </Grid>
            </Grid>
        );
    }
}

export default withSnackbar(withStyles(styles)(ProductDetail));