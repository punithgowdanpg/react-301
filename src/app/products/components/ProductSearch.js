import React, { Component } from 'react'
import Grid from '@material-ui/core/Grid';
import ProductCard from '../../shared/components/ProductCard/ProductCard'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Fab from '@material-ui/core/Fab';

import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Loader from '../../shared/components/Loader/Loader';
import queryString from 'query-string';
import history from '../../shared/utils/history';
const styles = theme => ({
  root: {
    width: "100%",
    paddingLeft: "1.5%"
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  extendedIcon: {
    margin: theme.spacing.unit,
    backgroundColor: "#1a1c23de",
    '&:hover': {
      backgroundColor: '#1a1c23de',
    }
  },
  name: {
    backgroundColor: "##eff0f100",
    '&:hover': {
      backgroundColor: '##eff0f100',
    }
  },
  noResult: {
    fontStyle: 'italic',
    textAlign: 'center'
  }
});
class ProductSearch extends Component {

  constructor(props) {
    super(props)
    this.props.clearProducts();
  }

  componentDidMount() {
    this._div.scrollTop = 0;
    let searchTxt = queryString.parse(window.location.search).search;
    this.props.getAllProducts(searchTxt);
  }

  renderProducts() {
    if (this.props.products.length === 0) {
      return <Loader />
    }
    else {
      return this.props.products.products.map(product => {
        return <Grid item xs={12} md={3} key={product.id} onClick={() => this.routeToProductDetails(product.id)}>
          <ProductCard product={product} />
        </Grid>
      })
    }

  }

  sort(propertyName, order) {
    this.props.sort(this.props.products, propertyName, order);
  }

  routeToProductDetails(productId) {
    this.props.clearProductDetails();
    history.push(`/products/${productId}/`)

  }

  renderProductsSorting(extendedClass, name) {
    if (this.props.products.length !== 0) {
      return <>
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('name', 'asc') }}
        >
          <span className={name}>
            A To Z
          </span>
          <ArrowDownward />
        </Fab>
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('name', 'desc') }}
        >
          <span className={name}>
            Z to A
          </span>
          <ArrowUpward />
        </Fab>

        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('buyoutPoints', 'asc') }}
        >
          <span className={name}>
            Points - Low to high
          </span>
          <ArrowUpward />
        </Fab>
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('buyoutPoints', 'desc') }}
        >
          <span className={name}>
            Points - High to low
          </span>
          <ArrowDownward />
        </Fab>



      </>
    }
  }
  render() {
    const { classes } = this.props;
    const { products } = this.props.products;
    return (
      <Grid container className={classes.root} spacing={24} alignItems="center"
        justify="center" style={{ height: "50vh" }} ref={(ref) => this._div = ref}

      >
        {products.length !== 0 && (
          <React.Fragment>
            <Grid item xs={12} >
              {this.renderProductsSorting(classes.extendedIcon, classes.name)}
            </Grid>
            {this.renderProducts()}
          </React.Fragment>
        )}
        {products.length === 0 && (
          <React.Fragment>
            <Grid item xs={12} >
              <Typography variant="h5" className={classes.noResult} gutterBottom>
                No Product found for the search text
              </Typography>
            </Grid>
          </React.Fragment>
        )}
        
      </Grid>
    )
  }
}




export default withStyles(styles)(ProductSearch);