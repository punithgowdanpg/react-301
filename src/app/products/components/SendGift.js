import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import ShoppingCart from '@material-ui/icons/ShoppingCart';
import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import moment from 'moment';
import { styled } from "@material-ui/styles";

import sendGiftEmail from '../../shared/utils/email';
import { withSnackbar } from 'notistack';
function Transition(props) {
    return <Slide direction="up" {...props} />;
}
const styles = theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
    },
    margin: {
        margin: theme.spacing.unit,
    },
    withoutLabel: {
        marginTop: theme.spacing.unit * 3,
    },
    textField: {
        flexBasis: 200,
    },
});

const SendGiftButton = styled(Button)({
    background: "#cc2929",
    boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)",
    border: 0,
    borderRadius: 3,
    color: "white",
    height: 48,
    padding: "0 30px",
    '& :hover': {
        color: '#000'
    }
  });
 
class SendGift extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            sendGiftTo: '',
            giftMessage: '',
            isFormError: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.formSubmit = this.formSubmit.bind(this);
    }


    handleClickOpen = () => {
        this.setState({ open: true });
    };

    handleClose = () => {
        this.setState({ open: false });
    };

    formSubmit(e) {
        e.preventDefault();
        const userPoints = this.props.user.rewardPoints;
        const giftPoints = parseInt(this.props.buyoutPoints);

        if(userPoints < giftPoints) {
            this.props.enqueueSnackbar("you don't have enough points to buy this gift", 
            { variant: 'info' });
            return;
        }
        
        if (this.handleValidation(this.state.sendGiftTo)) {
            //deduct in users 
            this.deductInRewards()
            //send Email

            this.sendEmail()

            // Store in sent details
            this.storeSentDetails()
            //Store in 

            this.sendReceiverDetails()

            this.handleClose();

            this.props.enqueueSnackbar('Successfully sent Gift', { variant: 'success' })
        }
    }
    sendEmail() {
        const templateParams = {
            toEmail: this.state.sendGiftTo,
            fromName: this.props.user.userName,
            giftName: this.props.name,
            giftMessage: this.state.giftMessage ? this.state.giftMessage : '',
        }
        sendGiftEmail(templateParams)

    }
    deductInRewards = () => {
        const payload = {
            "name": this.props.user.userName,
            "email": this.props.user.userEmail,
            "rewardPoints": this.props.user.rewardPoints - this.props.buyoutPoints,
            "isAdmin": this.props.user.isAdmin,
            "userImg": this.props.user.userImg
        }

        this.props.updateRewards(this.props.user.userId, payload)
    }

    storeSentDetails = () => {
        const payload = {
            "giftCardName": this.props.name,
            "brand": this.props.brand,
            "receiver": this.state.sendGiftTo,
            "message": this.state.giftMessage ? this.state.giftMessage : '',
            "sentDate": moment().toDate().toDateString(),
            "expiryDate": moment().add(this.props.expiryDays, 'days').toDate().toDateString()
        }
        this.props.sendGifts(this.props.user.userId, payload)
    }

    sendReceiverDetails = () => {
        const payload = {
            "giftCardName": this.props.name,
            "brand": this.props.brand,
            "sender": this.props.user.userName,
            "message": this.state.giftMessage ? this.state.giftMessage : '',
            "receivedDate": moment().toDate().toDateString(),
            "expiryDate": moment().add(this.props.expiryDays, 'days').toDate().toDateString(),
            "userId": this.state.sendGiftTo
        }
        this.props.receiveGifts(payload)
    }
    handleValidation = (email) => {
        let lastAtPos = email.lastIndexOf('@');
        let lastDotPos = email.lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && email.indexOf('@@') === -1 && lastDotPos > 2 && (email.length - lastDotPos) > 2)) {
            this.setState({ isFormError: true });
            return false
        }
        else {
            this.setState({ isFormError: false });
            return true;
        }

    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }


    render() {
        const { user } = this.props;
        return (
            <>

                <SendGiftButton disabled={!user.isSignedIn} variant="contained" size="large"  onClick={this.handleClickOpen}>
                    <ShoppingCart />
                    Gift Now
                </SendGiftButton>
                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    keepMounted
                    disableBackdropClick={true}
                    TransitionComponent={Transition}
                    aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Send Gift</DialogTitle>
                    <form noValidate autoComplete="off" onSubmit={this.formSubmit}>
                        <DialogContent>


                            <FormControl fullWidth={true} >
                                <TextField
                                    error={this.state.isFormError}
                                    id="standard-uncontrolled"
                                    label="Email"
                                    placeholder="Your email here"
                                    onChange={this.handleInputChange}
                                    margin="normal"
                                    type="email"
                                    name="sendGiftTo"
                                />
                            </FormControl >
                            <FormControl fullWidth={true} >
                                <TextField

                                    id="standard-error"
                                    label="Message"
                                    placeholder="Your message here!"
                                    onChange={this.handleInputChange}
                                    margin="normal"
                                    type="text"
                                    name="giftMessage"

                                />
                            </FormControl>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                                Cancel
            </Button>
                            <Button type="submit" color="primary">
                                Send Gift
            </Button>
                        </DialogActions>
                    </form>
                </Dialog>
            </>
        );
    }
}

export default withSnackbar(withStyles(styles)(SendGift));