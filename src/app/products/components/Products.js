import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import ProductCard from '../../shared/components/ProductCard/ProductCard';
import { withStyles } from '@material-ui/core/styles';
import history from '../../shared/utils/history';
import Fab from '@material-ui/core/Fab';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Loader from '../../shared/components/Loader/Loader';
import Breadcrumbs from '@material-ui/lab/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import { emphasize } from '@material-ui/core/styles/colorManipulator';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import axios from 'axios';

import Filter from '../containers/Filter'
const styles = theme => ({
  root: {
    width: "100%",

  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
  extendedIcon: {
    margin: theme.spacing.unit,
    backgroundColor: "#1a1c23de",
    '&:hover': {
      backgroundColor: '#1a1c23de',
    }
  },
  name: {
    backgroundColor: "##eff0f100",
    '&:hover': {
      backgroundColor: '##eff0f100',
    }
  }
});
const StyledBreadcrumb = withStyles(theme => ({
  root: {
    backgroundColor: theme.palette.grey[100],
    height: 24,
    color: theme.palette.grey[800],
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: theme.palette.grey[300],
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(theme.palette.grey[300], 0.12),
    },
  },
}))(Chip);
class Products extends Component {

  constructor(props) {
    super(props)
    this.state = {
      categoryName: ''
    }
    this.props.clearProducts();
    this.getCategoryName(this.props.categoryId)
  }

  componentDidMount() {

    this._div.scrollTop = 0

    this.props.getProducts(this.props.categoryId);

  }

  routeToProductDetails(productId) {
    this.props.clearProductDetails();
    history.push(`/products/${productId}/`)

  }
  handleCategories = () => {
    history.push('/categories');
  };
  handleHome = () => {
    history.push('/');
  };
  renderProducts() {
    if (this.props.products.length === 0) {
      return <Loader />
    }
    else {
      return this.props.products.map(product => {
        return <Grid item xs={12} md={3} key={product.id} onClick={() => this.routeToProductDetails(product.id)}>
          <ProductCard product={product} />
        </Grid>
      })
    }

  }

  sort(propertyName, order) {
    this.props.sort(this.props.products, propertyName, order);
  }
  getCategoryName = async (categoryId) => {
    const response = await axios.get(`https://myrewards7.azurewebsites.net/categories/${categoryId}`);
    this.setState({ categoryName: response.data.name })
    // console.log(response.data.name)
  };

  renderProductsSorting(extendedClass, name) {
    if (this.props.products.length !== 0) {
      return <>
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('name', 'asc') }}
        >
          <span className={name}>
            A to Z
          </span>
          <ArrowDownward />
        </Fab>
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('name', 'desc') }}
        >
          <span className={name}>
            Z to A
          </span>
          <ArrowUpward />
        </Fab>

        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('buyoutPoints', 'asc') }}
        >
          <span className={name}>
            Points - Low to High
          </span>
          <ArrowUpward />
        </Fab>
        <Fab
          variant="extended"
          size="small"
          color="primary"
          aria-label="Add"
          className={extendedClass}
          onClick={() => { this.sort('buyoutPoints', 'desc') }}
        >
          <span className={name}>
            Points - High to Low
          </span>
          <ArrowDownward />
        </Fab>
      </>
    }
  }


  render() {
    const { classes } = this.props;
    return (
      <div>
        <Paper elevation={0} className={classes.breadcrumb}>
          <Breadcrumbs aria-label="Breadcrumb">
            <StyledBreadcrumb
              component="a"
              href="#"
              label="Home"
              onClick={this.handleHome}
            />
            <StyledBreadcrumb component="a" href="#" label="Categories" onClick={this.handleCategories} />
            {this.state.categoryName ?
              <Typography variant="h6" color="inherit">
                {this.state.categoryName}
              </Typography> : <></>}

          </Breadcrumbs>
        </Paper>
        <Grid container className={classes.root} spacing={24} alignItems="center"
          justify="center" style={{ height: "50vh" }} ref={(ref) => this._div = ref}
        >
          <Grid item xs={12} >
            {this.renderProductsSorting(classes.extendedIcon, classes.name)}
          </Grid>
          <Grid container >
            <Grid item xs={12} md={2} gutterBottom>
              <Filter categoryId={this.props.categoryId} />
            </Grid>
            <Grid item xs={12} md={10}>
              <Grid container className={classes.root} spacing={24} alignItems="center"
                justify="center">
                {this.renderProducts()}
              </Grid>
            </Grid>
          </Grid>

        </Grid>

      </div>
    )
  }
}




export default withStyles(styles)(Products);