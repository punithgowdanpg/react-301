import React from 'react';
import { shallow } from 'enzyme';
import Filter from './Filter';
describe('Product Filter component', () => {
    test('should render Product Filter correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<Filter />);
        expect(wrapper).toMatchSnapshot();
    });
});