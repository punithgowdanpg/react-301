import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Checkbox from '@material-ui/core/Checkbox';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControl from '@material-ui/core/FormControl';
import axios from 'axios';
import config from '../../../config';
// eslint-disable-next-line no-unused-vars
const checkboxes = [
    {
        name: 'check-box-1',
        key: 'checkBox1',
        label: 'Check Box 1',
    },
    {
        name: 'check-box-2',
        key: 'checkBox2',
        label: 'Check Box 2',
    },
    {
        name: 'check-box-3',
        key: 'checkBox3',
        label: 'Check Box 3',
    },
    {
        name: 'check-box-4',
        key: 'checkBox4',
        label: 'Check Box 4',
    },
];

const styles = theme => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
});


class Filter extends Component {
    constructor(props) {
        super(props);
        this.getProducts(this.props.categoryId)
        this.state = {
            checkedItems: new Map(),
            // eslint-disable-next-line no-array-constructor
            checkedArray: new Array(),
        }
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        // console.log(this.props.products)
    }
    handleChange = (e) => {
        const item = e.target.name;
        const isChecked = e.target.checked;

        if (isChecked) {
            const cArray = [...this.state.checkedArray, item]
            this.filterBrand(cArray)
            this.setState({ checkedArray: cArray });

        }
        else {
            const cArray = this.state.checkedArray.filter(i => i !== item)
            this.setState({
                checkedArray: cArray
            })
            this.filterBrand(cArray)
        }


    }

    filterBrand(cArray) {
        if (cArray.length === 0) {
            // console.log("Calling this empty")
            this.props.FilterProducts(this.state.products)
        }
        else {
            // console.log("Calling this extra")

            this.props.FilterProducts(this.state.products.filter(i =>

                cArray.includes(i.brand))
            )
        }
    }

    handlePrice = (e) => {
        // console.log(e.target.value)
        if (e.target.value === '1000') {
            this.props.FilterProducts(this.state.products.filter(i => i.buyoutPoints < 1000
            ))
        }
        if (e.target.value === '5000') {
            this.props.FilterProducts(this.state.products.filter(i => i.buyoutPoints > 1000 & i.buyoutPoints < 5000
            ))
        }
        if (e.target.value === '10000') {
            this.props.FilterProducts(this.state.products.filter(i => i.buyoutPoints > 5000 & i.buyoutPoints < 10000
            ))
        }
        if (e.target.value === 'more') {
            this.props.FilterProducts(this.state.products.filter(i => i.buyoutPoints > 10000
            ))
        }
        if (e.target.value === 'all') {
            this.props.FilterProducts(this.state.products)
        }
    }

    getProducts = async (categoryId) => {

        const response = await axios.get(`${config.API_URL}/categories/${categoryId}/products`);
        this.setState({ products: response.data })
        const brands = response.data.map(function (obj) {
            return obj.brand;
        });
        const uniqueBrands = Array.from(new Set(brands))
        this.setState({ brands: uniqueBrands })

    };

    render() {
        const { classes } = this.props;

        return (
            <form className={classes.root}>
                <ExpansionPanel >
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                    >
                        <Typography className={classes.heading}>Brand</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>

                        <FormGroup>
                            {
                                this.state.brands && this.state.brands.map(brand => (

                                    <FormControlLabel
                                        control={
                                            <Checkbox name={brand} onChange={this.handleChange} value={brand} />
                                        }
                                        label={brand}
                                    />
                                ))
                            }
                        </FormGroup>

                    </ExpansionPanelDetails>
                </ExpansionPanel>
                <ExpansionPanel >
                    <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel2a-content"
                        id="panel2a-header"
                    >
                        <Typography className={classes.heading}>Price</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <FormControl component="fieldset" >

                            <RadioGroup
                                aria-label="gender"
                                name="gender2"


                                onChange={this.handlePrice}
                            >
                                <FormControlLabel
                                    value="1000"
                                    control={<Radio color="primary" />}
                                    label="0-1000"
                                    labelPlacement="end"
                                />
                                <FormControlLabel
                                    value="5000"
                                    control={<Radio color="primary" />}
                                    label="1000-5000"
                                    labelPlacement="end"
                                />
                                <FormControlLabel
                                    value="10000"
                                    control={<Radio color="primary" />}
                                    label="5000-10000"
                                    labelPlacement="end"
                                />
                                <FormControlLabel
                                    value="more"
                                    control={<Radio color="primary" />}
                                    label="10000+"
                                    labelPlacement="end"
                                />
                                <FormControlLabel
                                    value="all"
                                    control={<Radio color="primary" />}
                                    label="All Range"
                                    labelPlacement="end"
                                />
                            </RadioGroup>

                        </FormControl>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

            </form>
        );
    }
}
export default withStyles(styles)(Filter)