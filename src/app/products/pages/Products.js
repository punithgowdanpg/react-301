import React from 'react'
import Products from '../containers/Products'
export default function Categories(props) {
    return (
        <>
            {/* <SubHeader heading="Products" /> */}
            <Products categoryId={props.match.params.categoryId} />
        </>
    )
}
