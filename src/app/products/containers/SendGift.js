
import { connect } from "react-redux";

import SendGifts from "../components/SendGift";
import { updateRewards } from '../../auth/state/actions/userAction'
import { sendGifts, receiveGifts } from '../state/actions/sendGifts'
const mapStateToProps = (state) => ({
    user: state.auth,
    users: state.users
})

export default connect(mapStateToProps, {
    updateRewards,
    sendGifts,
    receiveGifts
})(SendGifts);