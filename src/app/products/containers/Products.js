
import { connect } from "react-redux";
import { getProducts, clearProducts, sort, clearProductDetails } from '../state/actions/productActions';
import Products from "../components/Products";

const mapStateToProps = (state) => ({
    
    products: state.products
})

export default connect(mapStateToProps, {
    getProducts,
    clearProducts,
    sort,
    clearProductDetails
})(Products);