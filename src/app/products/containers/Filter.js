
import { connect } from "react-redux";
import { getProducts, clearProducts, sort, clearProductDetails, FilterProducts } from '../state/actions/productActions';
import Filter from "../components/Filter";

const mapStateToProps = (state) => ({
    products: state.products
})

export default connect(mapStateToProps, {
    getProducts,
    clearProducts,
    sort,
    clearProductDetails,
    FilterProducts
})(Filter);


