
import { connect } from "react-redux";
import {
    getAllProducts,
    updateSearchTxt,
    clearProducts,
    sort,
    clearProductDetails
} from '../state/actions/productSearchActions';
import ProductSearch from "../components/ProductSearch";

const mapStateToProps = (state) => ({
    products: state.productSearch
})

export default connect(mapStateToProps, {
    getAllProducts,
    updateSearchTxt,
    clearProducts,
    sort,
    clearProductDetails
})(ProductSearch);