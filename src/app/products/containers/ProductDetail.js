import {connect} from "react-redux";
import ProductDetail from "../components/ProductDetail";
import { 
    getProductDetail, 
    updateRating,
    updateComment, 
    getProductComments ,
    addFavorites,
    removeFavorites
} from "../state/actions/productDetailAction";
import { getFavourites } from "../../favourites/store/actions/favouritesAction";

const mapStateToProps = state => {
    return {
        productDetail: state.productDetail.products,
        productComments: state.productDetail.comments,
        favourites: state.favourites,
        auth: state.auth
    };
};
  
export default connect(mapStateToProps, {
    getProductDetail,
    updateRating,
    updateComment,
    getProductComments,
    addFavorites,
    removeFavorites,
    getFavourites
})(ProductDetail);