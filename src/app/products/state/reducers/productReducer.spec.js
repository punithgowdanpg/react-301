import productReducer from './productReducer'

const products = [
    {
        "name": "MakeMyTrip Gift Card",
        "brand": "MakeMyTrip",
        "desc": " This Gift card is valid for a period of 12 Months from the date of Purchase.",
        "imageUrl": "https://images-na.ssl-images-amazon.com/images/I/41OHN9BVORL.jpg",
        "buyoutPoints": "10000",
        "expiryDays": "365",
        "categoryId": "7",
        "rating": 4,
        "id": "AyH3zKM"
    },
    {
        "name": "MakeMyTrip Happy Wedding Gift Card",
        "brand": "MakeMyTrip",
        "desc": " The Gift card is valid for purchases made from Makemytrip only for Flights.",
        "imageUrl": "https://images-na.ssl-images-amazon.com/images/I/4174hFSDQSL.jpg",
        "buyoutPoints": "10000",
        "expiryDays": "365",
        "categoryId": "7",
        "rating": 4,
        "id": "a1tYKdu"
    }
];

describe('Product Reducer', () => {
    it('should setup default value', () => {
        const state = productReducer(undefined, { type: '@@INIT'});
        expect(state).toEqual([]);
    });
});