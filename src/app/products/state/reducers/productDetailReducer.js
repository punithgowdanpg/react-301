import { TYPES } from "../../../shared/constants/actionTypes";


export const INITIAL_STATE = {
    products: {
        name: '',
        brand: '',
        desc: '',
        imageUrl: '',
        buyoutPoints: '',
        expiryDays: '',
        categoryId: '',
        rating: '',
        id: ''
    },
    comments: [],
    wishList: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.CLEAR_PRODUCT_DETAILS:
            return INITIAL_STATE;
        case TYPES.FETCH_PRODUCT_DETAIL:
            return { ...state, products: action.payload };
        case TYPES.UPDATE_PRODUCT_DETAIL:
            return { ...state, products: action.payload };
        case TYPES.FETCH_PRODUCT_COMMENTS:
            return { ...state, comments: action.payload };
        case TYPES.UPDATE_PRODUCT_COMMENTS:
            return Object.assign({}, state, {
                comments: state.comments.concat(action.payload)
            });
        default:
            return state;
    }
};