import productDetailReducer, { INITIAL_STATE } from './productDetailReducer'
import { TYPES } from "../../../shared/constants/actionTypes";

describe('Product Detail Reducer', () => {
    it('should setup default value', () => {
        const state = productDetailReducer(undefined, { type: '@@INIT'});
        expect(state).toEqual(INITIAL_STATE);
    });

    it('clear product type in reducer', () => {
        const state = productDetailReducer(null, { type: TYPES.CLEAR_PRODUCT_DETAILS });
        expect(state).toEqual(INITIAL_STATE);
    });

});