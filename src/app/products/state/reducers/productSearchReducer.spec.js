import productSearchReducer, { INITIAL_STATE } from './productSearchReducer'

describe('Product Search Reducer', () => {
    test('should setup default value', () => {
        const state = productSearchReducer(undefined, { type: '@@INIT'});
        expect(state).toEqual(INITIAL_STATE);
    });
});