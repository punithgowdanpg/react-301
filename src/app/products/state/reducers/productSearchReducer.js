import { TYPES } from "../../../shared/constants/actionTypes";

export const INITIAL_STATE = {
    products: [],
    searchTxt: ''
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.FETCH_PRODUCTS:
            return { ...state, products: action.payload };
        case TYPES.CLEAR_PRODUCTS:
            return { ...state, products: action.payload };
        case TYPES.UPDATE_SEARCH:
            return { ...state, searchTxt: action.searchTxt };
        default:
            return state;
    }
};