import axios from 'axios';
import { TYPES } from '../../../shared/constants/actionTypes';
import config from '../../../../config';
export const getAllProducts = (searchText) => async (dispatch, getState) => {

    const response = await axios.get(`${config.API_URL}/products?q=${searchText}`);
    dispatch({
        type: TYPES.FETCH_PRODUCTS,
        payload: response.data
    });
};

export const updateSearchTxt = (searchText) => async (dispatch, getState) => {
    dispatch({
        type: TYPES.UPDATE_SEARCH,
        searchText
    });
};

export const clearProducts = () => (dispatch, getState) => {
    dispatch({
        type: TYPES.CLEAR_PRODUCTS,
        payload: []
    });
}

export const sort = (currentProducts, sortProperty, sortOrder) => (dispatch, getState) => {
    if (sortOrder !== 'asc') {
        sortProperty = '-' + sortProperty;
    }
    const sortedProducts = [...currentProducts.products].sort(dynamicSort(sortProperty))

    dispatch({
        type: TYPES.FETCH_PRODUCTS,
        payload: [...sortedProducts]
    });
}

const dynamicSort = (property) => {
    var sortOrder = 1;

    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    if (property === 'buyoutPoints') {
        return function (a, b) {
            if (sortOrder === -1) {
                return (parseInt(a[property]) < parseInt(b[property])) ? 1 : (parseInt(a[property]) > parseInt(b[property])) ? -1 : 0;

            } else {
                return (parseInt(a[property]) < parseInt(b[property])) ? -1 : (parseInt(a[property]) > parseInt(b[property])) ? 1 : 0;
            }
        }
    }
    return function (a, b) {
        if (sortOrder === -1) {
            return b[property].localeCompare(a[property]);
        } else {
            return a[property].localeCompare(b[property]);
        }
    }
}
export const clearProductDetails = () => (dispatch, getState) => {
    dispatch({
        type: TYPES.CLEAR_PRODUCT_DETAILS,
        payload: []
    });
}