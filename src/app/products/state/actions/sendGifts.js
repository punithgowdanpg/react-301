import axios from 'axios';
import { TYPES } from '../../../shared/constants/actionTypes';

export const sendGifts = (userID, giftObject) => async (dispatch, getState) => {
    const response = await axios.post(`https://myrewards7.azurewebsites.net/users/${userID}/sentGifts`, giftObject);
    dispatch({
        type: TYPES.ADD_SENT_GIFTS,
        payload: response.data
    });
};

export const receiveGifts = ( giftObject) => async (dispatch, getState) => {
    const response = await axios.post(`https://myrewards7.azurewebsites.net/receivedGifts`, giftObject);
    dispatch({
        type: TYPES.ADD_RECEIVED_GIFTS,
        payload: response.data
    });
};