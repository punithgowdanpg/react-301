import axios from 'axios';
import { TYPES } from '../../../shared/constants/actionTypes';
import config from '../../../../config';
export const getProductDetail = (productId) => async (dispatch, getState) => {
    const response = await axios.get(`${config.API_URL}/products/${productId}`);
    dispatch({
        type: TYPES.FETCH_PRODUCT_DETAIL,
        payload: response.data
    });
};

export const getProductComments = (productId) => async (dispatch, getState) => {
    const response = await axios.get(`${config.API_URL}/product/${productId}/comments`);
    dispatch({
        type: TYPES.FETCH_PRODUCT_COMMENTS,
        payload: response.data
    });
};

export const updateRating = (productId, currentUser, rating, reviewedAt) => async (dispatch, getState) => {
    let productDetail = getState().productDetail.products;
    let currentRating = '';
    if (productDetail.rating === "" || productDetail.rating === null || productDetail.rating === undefined) {
        currentRating = rating
    } else {
        currentRating = Math.ceil((rating + parseInt(productDetail.rating)) / 2);
    }
    productDetail = { ...productDetail, rating: currentRating };
    const response = await axios.put(`${config.API_URL}/products/${productId}`, productDetail);
    dispatch({
        type: TYPES.UPDATE_PRODUCT_DETAIL,
        payload: response.data
    });
};

export const updateComment = (productId, comment, commentAt) => async (dispatch, getState) => {
    let commentInfo = {
        userEmail: getState().auth.userEmail,
        userName: getState().auth.userName,
        comment: comment,
        commentedAt: commentAt
    };
    const response = await axios.post(`${config.API_URL}/product/${productId}/comments`, commentInfo);
    dispatch({
        type: TYPES.UPDATE_PRODUCT_COMMENTS,
        payload: response.data
    });
};

export const addFavorites = (productName, imageUrl, buyoutPoints, ProductId,
    addedAt, userId) => async (dispatch, getState) => {
        let favData = {
            productName,
            imageUrl,
            buyoutPoints,
            id: ProductId,
            addedAt,
            userId
        };
        const response = await axios.post(`${config.API_URL}/wishlist`, favData);
        dispatch({
            type: TYPES.ADD_FAVOURITES,
            payload: response.data
        });
    };

export const removeFavorites = (productId) => async (dispatch, getState) => {
    const response = await axios.delete(`${config.API_URL}/wishlist/${productId}`);
    dispatch({
        type: TYPES.REMOVE_FAVOURITES,
        payload: productId
    });
};
