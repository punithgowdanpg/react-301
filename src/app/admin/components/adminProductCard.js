import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    // maxWidth: 345,
    height: "100%"
  },
  media: {
    height: 140,
    margin: "5% 2%"
  },
  content:{
    height:'85%'
  }
};

function ProductCard(props) {
  const { classes, data, deleteProduct, edit } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea className={classes.content}>
        <CardMedia
          className={classes.media}
          image={data.imageUrl}
          title={data.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h6" component="h2">
          {data.name}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={()=>{edit(data)}}>
          Edit
        </Button>
        <Button size="small" color="primary" onClick={()=>{deleteProduct(data.id)}}>
          Delete
        </Button>
      </CardActions>
    </Card>
  );
}

ProductCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ProductCard);