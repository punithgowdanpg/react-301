import React from 'react';
import { shallow } from 'enzyme';
import Navigation from './navigation';
describe('Admin Navigation component', () => {
    test('should render Admin Navigation Panel correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<Navigation />);
        expect(wrapper).toMatchSnapshot();
    });
});