import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { withStyles } from '@material-ui/core/styles';
import ListItemText from '@material-ui/core/ListItemText';
import AssessmentIcon from '@material-ui/icons/Assessment';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom'

const drawerWidth = 100;

const styles = theme => ({
    root: {
      display: 'flex',
      color: "red !important"
    },
    appBar: {
        marginLeft: drawerWidth,
        background: "linear-gradient(45deg, #1a1c23 30%, #cc2929 90%)",
        [theme.breakpoints.up('sm')]: {
          display: "none"
        },
      },
    drawer: {
      [theme.breakpoints.up('sm')]: {
        width: "100%",
        flexShrink: 0,
      },
    },
    menuButton: {
      marginRight: "2%",
      [theme.breakpoints.up('sm')]: {
        display: 'none',
      },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
      width: "70%",
      position: "inherit",
      [theme.breakpoints.up('sm')]: {
        width: "16%",
        top: "5%",
        zIndex:"0",
        height: "100%",
        position: "fixed",
        flexShrink: 0,
      },
    },
    content: {
      flexGrow: 1,
      padding: "3%",
    },
    link: {
        textDecoration: 'none',
        color: '#fff',
        [theme.breakpoints.up('sm')]: {
          display: 'block',
        },
      },
  });


function Navigation(props) {
  const { container } = props;
  const { classes } = props;
//   const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);

  function handleDrawerToggle() {
    setMobileOpen(!mobileOpen);
  }

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        <Link className={classes.link} to="/admin/">
        <ListItem button>
        <ListItemIcon><AssessmentIcon /></ListItemIcon>
        <ListItemText primary={"Dashboard"} />
      </ListItem>
      </Link>
      <Link className={classes.link} to="/admin/categories">
        <ListItem button>
        <ListItemIcon><AssessmentIcon /></ListItemIcon>
        <ListItemText primary={"Categories"} />
      </ListItem>
      </Link>
      <Link className={classes.link} to="/admin/allProducts">
        <ListItem button>
        <ListItemIcon><AssessmentIcon /></ListItemIcon>
        <ListItemText primary={"Products"} />
      </ListItem>
      </Link>
      
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
    <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="Open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap style={{color: "#fff"}}>
           Admin Dashboard
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer}>
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
              paperAnchorLeft: classes.left
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
}

Navigation.propTypes = {
  // Injected by the documentation to work in an iframe.
  // You won't need it on your project.
  container: PropTypes.object,
};
export default withStyles(styles)(Navigation);
// export default Navigation;
