import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Formik } from 'formik';
import TextField from "@material-ui/core/TextField";
import Button from '@material-ui/core/Button';
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Grid from '@material-ui/core/Grid';


const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    padding: "0 5%",
    [theme.breakpoints.down('md')]: {
      overflow: "hidden",
      overflowY: "scroll"
    },
  },
  title: {
    textAlign: "center"
  },
  img: {
    width: "92%",
    height: 230,
    minWidth: "92%",
    minHeight: 230,
    margin: "6% 8% 0 0",
    border: "1px solid",
    boxShadow: "5px 5px",
    [theme.breakpoints.down('md')]: {
      minHeight: "unset",
      margin: "6% 8% 0 4%",
      height: "100px"
    },
  },
  noImg: {
    width: "90%",
    height: 200,
    margin: "6% 8% 0 0",
    textAlign: "center",
    border: "1px solid",
    boxShadow: "5px 5px",
    [theme.breakpoints.down('md')]: {
      minHeight: "unset",
      margin: "6% 8% 0 4%",
      height: "100px"
    },
  },
  noImgSpan: {
    verticalAlign: "middle",
    lineHeight: 12,
    display: 'inline-block',
    [theme.breakpoints.down('md')]: {
      lineHeight: 6
    },
  },
  paper: {
    width: "45%",
    maxWidth: "none",
    height: 350,
    overflow: "hidden",
    [theme.breakpoints.down('md')]: {
      width:"70%",
    },
  },
  button: {
    margin: "8% 2%",
    [theme.breakpoints.down('md')]: {
      margin: "8% 5%"
    },
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 3,
    width: 200,
    [theme.breakpoints.down('md')]: {
      width: "180px"
    },
  },
});

  function EditCategory(props) {

  function handleClose() {
    props.onClose();
  };

    const { classes, onClose, theme, data, add,handleUpdate, ...other } = props;
    const [imageURL, setImageURL] = React.useState(props.data.imageURL);

    return (
      <Dialog
      onClose={handleClose}
      classes={{ paper: classes.paper }}
      aria-labelledby="simple-dialog-title"
      {...other}
    >
      <DialogTitle id="simple-dialog-title" classes={{ root: classes.title }}>
      {add ? "Add" : "Edit"} Category
      </DialogTitle>
      <div className={classes.root}>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={7} md={7}>
        {!imageURL ? <div className={classes.noImg}><span className={classes.noImgSpan}>Add New Image</span> </div> : 
        <img className={classes.img} src={imageURL} alt={data.name}/>}
        </Grid>
        <Grid item xs={12} sm={5} md={5}>
      <Formik
        initialValues={{ id: data.id, name: data.name, imageURL: imageURL }}
        isSubmitting={true}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            handleUpdate(values.id,add,values.name, imageURL);
            actions.resetForm ();
            props.onClose();
            actions.setSubmitting(false);
          }, 1000);
        }}
        render={props => (
          <form onSubmit={props.handleSubmit}>
          <TextField
          label="Category Name"
          className={classes.textField}
          margin="normal"
          id="name"
          type="text"
          value={props.values.name}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required
        />
          <TextField
          label="Image URL"
          className={classes.textField}
          margin="normal"
          id="imageURL"
          type="text"
          value={props.values.imageURL}
          onChange={props.handleChange}
          onBlur={()=>{setImageURL(props.values.imageURL)}}
          required
        />
        <br/>
        <Button variant="contained" color="primary" className={classes.button} onClick={handleClose} type="button">Cancel</Button>
        <Button variant="contained" color="primary" className={classes.button} type="submit">{add ? "Add" : "Update"}</Button>
          </form>
        )}
      />
      </Grid>
      </Grid>
      </div>
      </Dialog>
    );
}

EditCategory.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};


export default withStyles(styles, { withTheme: true })(EditCategory);
