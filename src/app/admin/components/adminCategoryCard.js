import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    // maxWidth: 345,
  },
  media: {
    height: 140,
  },
};

function CategoryCard(props) {
  const { classes, data, edit, deleteCategory } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={data.imageURL}
          title={data.name}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
          {data.name}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={()=>{edit(props.data)}}>
          Edit
        </Button>
        <Button size="small" color="primary" onClick={()=>{deleteCategory(props.data.id)}}>
          Delete
        </Button>
      </CardActions>
    </Card>
  );
}

CategoryCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CategoryCard);