import React from 'react';
import { shallow } from 'enzyme';
import EditCategory from './editCategory';
describe('Admin Edit Category component', () => {
    test('should render Categories Edit Modal for Admin correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<EditCategory />);
        expect(wrapper).toMatchSnapshot();
    });
});