import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ProductCard from './adminProductCard';
import EditProduct from './editproduct';
import Loader from '../../shared/components/Loader/Loader';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { withSnackbar } from 'notistack';


const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    margin: 0,
    padding: "2%"
  },
  filterRow: {
    width: "100%"
  },
  text: {
    width: "12%",
    display: "inline-block",
    float: "left"
  },
  button: {
    display: "inline-block",
    float: "right",
    marginRight: "1%",
    background: "rgba(26, 28, 35, 0.8)",
    '&:hover': {
      background: "rgba(26, 28, 35, 0.6)",
    },
    [theme.breakpoints.down('md')]: {
      width:"35%",
    },
  }, 
  textField: {
    width: "12%",
    marginLeft: "1%",
    background: "rgba(26, 28, 35, 0.8)",
    borderRadius: 4,
    [theme.breakpoints.down('md')]: {
      width:"50%",
    },
  },
  select: {
    color: "#fff",
    paddingLeft: "4%"
  },
  icon : {
    color: "#fff"
  }
});

class AdminProducts extends React.Component {
  state = {
    open: false,
    add: true,
    categoryId: "0",
    selected: {id:0,name: "",brand:"",desc:"", imageUrl: "",buyoutPoints:"",expiryDays:"",categoryId:"", rating:0}
  };
  componentDidMount() {
    this.props.getAdminProducts();  
    this.props.getAdminCategory();  
  }

  handleUpdate = (add,id,data) => {
    let newProduct = {...data};
    if(add){
      delete(newProduct.id);
      delete(newProduct.categoryId);
      delete(newProduct.rating);
      this.props.addProduct(id , newProduct);
      this.props.enqueueSnackbar("Product Added Successfully!", 
            { variant: 'success' })
    }
    else {
      delete(newProduct.id);
      this.props.updateProduct(data.id , newProduct);
      this.props.enqueueSnackbar("Product Updated Successfully!", 
            { variant: 'success' })
    }
   }

  handleModalOpen = () => {
    this.setState({ selected: {id:0,name: "",brand:"",desc:"", imageUrl: "",buyoutPoints:"",expiryDays:"",categoryId:"", rating:0}, add: true, open : true})
  }
  handleModalClose = () => {
    this.setState({ open: false, add: true });
  };

  handleSelect = (selected) => {
    this.setState({selected: selected, open: true, add: false })
  }

  
  handleDelete = (id) => {
    this.props.removeProduct(id);
    this.props.enqueueSnackbar("Category Deleted Successfully!", 
            { variant: 'success' })
  }
  handleChange = (categoryId)=>{
    categoryId === 0 ? this.props.getAdminProducts()   :
    this.props.getCategoryProducts(categoryId);
    this.setState({
      categoryId: categoryId
    })
  }
  render() {
    const { classes } = this.props;
    const { open, selected, add, categoryId } = this.state;
    if(this.props.allProducts.length === 0 ){
      return (
        <Grid container alignItems="center"
        justify="center" style={{ height: "90vh" }} className={classes.root} spacing={16}>
        <Loader/>
        </Grid>
      )
    }
    else {
    return (
      <Grid container className={classes.root} spacing={16}>
      <div className={classes.filterRow}>
     <FormControl className={classes.textField} >
      <Select
        value={categoryId}
        className={classes.select}
        classes={{icon : classes.icon}}
        onChange={(e)=>{this.handleChange(e.target.value)}}
        inputProps={{
          name: 'categoryId',
          id: 'categoryId',
        }}
      >
      <MenuItem key={0} value={0}>All Categories</MenuItem>
        {this.props.allCategories.map((item)=>{
          return (
            <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
          )
        })}
          
      </Select>
    </FormControl>
      <Button variant="contained" color="primary" className={classes.button} type="button" onClick={()=>{this.handleModalOpen()}}>Add Product</Button>
      </div>
        <Grid item xs={12}>
          <Grid container className={classes.demo} justify="center" spacing={32}>
            {this.props.allProducts.map(value => (
              <Grid key={value.id} item xs={12} sm={3}>
              <ProductCard data={value} deleteProduct={this.handleDelete} edit={this.handleSelect}/>
              </Grid>
            ))}
          </Grid>
        </Grid>
        {open && <EditProduct
        add={add}
        open={open}
        categories={this.props.allCategories}
        data={selected}
          onClose={this.handleModalClose}
          handleUpdate={this.handleUpdate}
          />
        }
      </Grid>
    );
      }
}
}

AdminProducts.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};


export default withSnackbar(withStyles(styles, { withTheme: true })(AdminProducts));
