import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CategoryCard from './adminCategoryCard'
import EditCategory from './editCategory'
import Loader from '../../shared/components/Loader/Loader';
import { withSnackbar } from 'notistack';


const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    margin: 0,
    padding: "2%"
  },
  filterRow: {
    width: "100%"
  },
  button: {
    display: "inline-block",
    width:"12%",
    float: "right",
    marginRight: "1%",
    background: "rgba(26, 28, 35, 0.8)",
    '&:hover': {
      background: "rgba(26, 28, 35, 0.6)",
    },
    [theme.breakpoints.down('md')]: {
      width:"35%",
    },
  }, 
});

class AdminCategories extends React.Component {
  state = {
    open: false,
    add: true,
    selected: {id:0,name: "", imageURL: ""}
  };
  componentDidMount() {
    this.props.getAdminCategory();  
  }

  handleUpdate = (id,add,name, imageURL) => {
    if(add){
      this.props.addCategory((this.props.allCategories.length+1).toString(), name, imageURL)
      this.props.enqueueSnackbar("Category Added Successfully!", 
            { variant: 'success' })
    }
    else {
      this.props.updateCategory(id, name, imageURL);
      this.props.enqueueSnackbar("Category Updated Successfully!", 
            { variant: 'success' })
    }
  }

  handleModalOpen = () => {
    this.setState({ selected: {id:0,name: "", imageURL: ""}, add: true, open : true})
  }
  handleModalClose = () => {
    this.setState({ open: false });
  };

  handleSelect = (selected) => {
    this.setState({selected: selected, open: true, add: false })
  }

  
  handleDelete = (id) => {
    this.props.removeCategory(id);
    this.props.enqueueSnackbar("Category Deleted Successfully!", 
            { variant: 'success' })
  }

  render() {
    const { classes } = this.props;
    const { open, selected, add } = this.state;
    if(this.props.allCategories.length === 0 ){
      return (
        <Grid container alignItems="center"
        justify="center" style={{ height: "90vh" }} className={classes.root} spacing={16}>
        <Loader/>
        </Grid>
      )
    }
    else {
    return (
      <Grid container className={classes.root} spacing={16}>
      <div className={classes.filterRow}>
      <Button variant="contained" color="primary" className={classes.button} type="button" onClick={()=>{this.handleModalOpen()}}>Add Category</Button>
      </div>
        <Grid item xs={12}>
          <Grid container className={classes.demo} justify="center" spacing={32}>
            {this.props.allCategories.map(value => (
              <Grid key={value.id} item xs={12} sm={3}>
              <CategoryCard data={value} deleteCategory={this.handleDelete} edit={this.handleSelect}/>
              </Grid>
            ))}
          </Grid>
        </Grid>
        {open && <EditCategory
        add={add}
        open={open}
        data={selected}
          onClose={this.handleModalClose}
          handleUpdate={this.handleUpdate}
          />
        }
      </Grid>
    );
      }
  }
}

AdminCategories.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};


export default withSnackbar(withStyles(styles, { withTheme: true })(AdminCategories));
