import React from 'react';
import { shallow } from 'enzyme';
import EditProduct from './editProduct';
describe('Admin Edit Product component', () => {
    test('should render Product Edit Modal for Admin correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<EditProduct />);
        expect(wrapper).toMatchSnapshot();
    });
});