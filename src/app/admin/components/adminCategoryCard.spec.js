import React from 'react';
import { shallow } from 'enzyme';
import CategoryCard from './adminCategoryCard';
describe('Admin Category Card component', () => {
    test('should render Category Cards in Admin correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<CategoryCard />);
        expect(wrapper).toMatchSnapshot();
    });
});