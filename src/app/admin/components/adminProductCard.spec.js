import React from 'react';
import { shallow } from 'enzyme';
import ProductCard from './adminProductCard';
describe('Admin Product Card component', () => {
    test('should render Product Cards in Admin correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<ProductCard />);
        expect(wrapper).toMatchSnapshot();
    });
});