import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Formik } from 'formik';
import TextField from "@material-ui/core/TextField";
import Button from '@material-ui/core/Button';
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';


const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    padding: "0 5%",
    overflowY: "scroll"
  },
  title: {
    textAlign: "center"
  },
  img: {
    width: "30%",
    height: 230,
    minWidth: "30%",
    minHeight: 230,
    margin: "0",
    position: "absolute",
    border: "1px solid",
    boxShadow: "5px 5px",
    [theme.breakpoints.down('md')]: {
      minHeight: "unset",
      width: "100%",
      height: "120px",
      position: "unset"
    }
  },
  detailGrid: {
    [theme.breakpoints.down('md')]: {
      marginTop: 30
    },
  },
  noImg: {
    width: "30%",
    height: 200,
    position: "absolute",
    margin: "0",
    textAlign: "center",
    border: "1px solid",
    boxShadow: "5px 5px", 
    [theme.breakpoints.down('md')]: {
      position: "unset",
      minHeight: "unset",
      width: "100%",
      height: "120px",
    },
  },
  noImgSpan: {
    verticalAlign: "middle",
    lineHeight: 12,
    display: 'inline-block',
    [theme.breakpoints.down('md')]: {
      lineHeight : 6
    },
  },
  paper: {
    width: "75%",
    maxWidth: "none",
    height: "80%",
    overflow: "hidden",
    [theme.breakpoints.down('md')]: {
      width: "90%",
      margin : 0
    },
  },
  button: {
    margin: "8% 2%"
  },
  textField: {
    margin: "1% 4%",
    marginBottom: theme.spacing.unit * 3,
    width: "40%",
    [theme.breakpoints.down('md')]: {
      width: "90%"
    },
  }
});

  function EditProduct(props) {

  function handleClose() {
    props.onClose();
  };

    const { classes, onClose, theme, data, add,handleUpdate,categories, ...other } = props;
    const [imageUrl, setImageUrl] = React.useState(props.data.imageUrl);
    return (
      <Dialog
      onClose={handleClose}
      classes={{ paper: classes.paper }}
      aria-labelledby="simple-dialog-title"
      {...other}
    >
      <DialogTitle id="simple-dialog-title" classes={{ root: classes.title }}>
      {add ? "Add" : "Edit"} Product
      </DialogTitle>
      <div className={classes.root}>
      <Grid container spacing={3} className={classes.details}>
        <Grid item xs={12} sm={4} md={4}>
        {!imageUrl ? <div className={classes.noImg}><span className={classes.noImgSpan}>Add New Image</span> </div> : 
        <img className={classes.img} src={imageUrl} alt={data.name}/>}
        </Grid>
        <Grid  item xs={12} sm={8} md={8} className={classes.detailGrid} style={{paddingLeft:"2%"}}>
      <Formik
        initialValues={{ id: data.id, name: data.name, brand: data.brand, desc:data.desc, imageUrl: imageUrl, buyoutPoints: data.buyoutPoints, expiryDays: data.expiryDays, categoryId: data.categoryId }}
        isSubmitting={true}
        onSubmit={(values, actions) => {
          setTimeout(() => {
            handleUpdate(add,values.categoryId,values);
            actions.resetForm ();
            props.onClose();
            actions.setSubmitting(false);
          }, 1000);
        }}
        render={props => (
          <form onSubmit={props.handleSubmit}>
          <TextField
          label="Product Name"
          className={classes.textField}
          margin="normal"
          id="name"
          type="text"
          multiline
          value={props.values.name}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required
        />
          <TextField
          label="Brand"
          className={classes.textField}
          margin="normal"
          id="brand"
          type="text"
          value={props.values.brand}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required
        />
          <TextField
          label="Description"
          className={classes.textField}
          margin="normal"
          id="desc"
          type="text"
          multiline
          style={{width: "90%"}}
          value={props.values.desc}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required
        />
          <TextField
          label="Image URL"
          multiline
          className={classes.textField}
          margin="normal"
          id="imageUrl"
          type="text"
          value={props.values.imageUrl}
          onChange={props.handleChange}
          onBlur={()=>{setImageUrl(props.values.imageUrl)}}
          required
        />
        <FormControl className={classes.textField} required>
        <InputLabel htmlFor="categoryId">Category</InputLabel>
        <Select
          value={props.values.categoryId}
          onChange={props.handleChange}
          inputProps={{
            name: 'categoryId',
            id: 'categoryId',
          }}
        >
      
          {categories.map((item)=>{
            return (
              <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
            )
          })}
            
        </Select>
      </FormControl>
          <TextField
          label="Buyout Points"
          className={classes.textField}
          margin="normal"
          id="buyoutPoints"
          type="text"
          value={props.values.buyoutPoints}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required
        />
          <TextField
          label="Expiry Days"
          className={classes.textField}
          margin="normal"
          id="expiryDays"
          type="text"
          value={props.values.expiryDays}
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          required
        />
         
        <br/>
        <Button variant="contained" color="primary" className={classes.button} onClick={handleClose} type="button">Cancel</Button>
        <Button variant="contained" color="primary" className={classes.button} type="submit">{add ? "Add" : "Update"}</Button>
          </form>
        )}
      />
      </Grid>
      </Grid>
      </div>
      </Dialog>
    );
}

EditProduct.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};


export default withStyles(styles, { withTheme: true })(EditProduct);
