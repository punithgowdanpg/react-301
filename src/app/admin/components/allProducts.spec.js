import React from 'react';
import { shallow } from 'enzyme';
import AdminProducts from './allProducts';
describe('Admin Products component', () => {
    test('should render Products Page for Admin correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<AdminProducts />);
        expect(wrapper).toMatchSnapshot();
    });
});