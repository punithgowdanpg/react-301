import React from 'react';
import { shallow } from 'enzyme';
import AdminCategories from './allCategories';
describe('Admin Categories component', () => {
    test('should render Categories Page for Admin correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<AdminCategories />);
        expect(wrapper).toMatchSnapshot();
    });
});