import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';
import Navigation from '../components/navigation'
const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: '100%'
    }
});

class AdminHome extends React.Component {


    render() {
        return (
            <React.Fragment>
            <Grid container spacing={3}>
            <Grid item xs={12} md={2}>
            <Navigation/>
            </Grid>
            <Grid item xs={12} md={10}>
            {this.props.children} 
</Grid>
</Grid>
            </React.Fragment>
        );
    }
};

export default connect(null,
    null
)(withStyles(styles)(AdminHome));