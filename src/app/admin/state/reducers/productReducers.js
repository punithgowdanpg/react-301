import { TYPES } from "../../../shared/constants/actionTypes";


const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.FETCH_ADMIN_PRODUCT:
            return [...action.payload];
        case TYPES.ADD_PRODUCT:
            return  [...state, action.payload ]
        default:
            return state;
    }
};