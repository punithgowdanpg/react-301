import axios from 'axios';
import { TYPES } from "../../../shared/constants/actionTypes";
import config from '../../../../config';


export const getAdminProducts = (formValues) => async (dispatch, getState) => {

    const response = await axios.get(`${config.API_URL}/products`);
    dispatch({
        type: TYPES.FETCH_ADMIN_PRODUCT,
        payload: response.data
    });
};

export const getCategoryProducts = (categoryId) => async (dispatch, getState) => {
    const response = await axios.get(`${config.API_URL}/categories/${categoryId}/products`);
    dispatch({
        type: TYPES.FETCH_ADMIN_PRODUCT,
        payload: response.data
    });
};

export const addProduct = (categoryId, data) => async (dispatch, getState) => {
    const response = await axios.post(`${config.API_URL}/categories/${categoryId}/products`, data);
   dispatch({
        type: TYPES.ADD_PRODUCT,
        payload: response.data
    });
};

export const updateProduct = (id, data) => async (dispatch, getState) => {

    await axios.put(`${config.API_URL}/products/${id}`, data);
    const response = await axios.get(`${config.API_URL}/products`);
    dispatch({
        type: TYPES.FETCH_ADMIN_PRODUCT,
        payload: response.data
    });
};

export const removeProduct = (id) => async (dispatch, getState) => {
    await axios.delete(`${config.API_URL}/products/${id}`);
    const response = await axios.get(`${config.API_URL}/products`);
    dispatch({
        type: TYPES.FETCH_ADMIN_PRODUCT,
        payload: response.data
    });
};