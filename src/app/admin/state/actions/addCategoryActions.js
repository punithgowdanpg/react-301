import axios from 'axios';
import { TYPES } from "../../../shared/constants/actionTypes";
import config from '../../../../config';


export const getAdminCategory = (formValues) => async (dispatch, getState) => {

    const response = await axios.get(`${config.API_URL}/categories`);
    dispatch({
        type: TYPES.FETCH_ADMIN_CATEGORIES,
        payload: response.data
    });
};

export const addCategory = (id, name,imageURL) => async (dispatch, getState) => {
    const newCategory = {
        "id": id,
        "name": name,
        "imageURL": imageURL
    };
    const response = await axios.post(`${config.API_URL}/categories`, newCategory);
   dispatch({
        type: TYPES.ADD_CATEGORY,
        payload: response.data
    });
};

export const updateCategory = (id, name,imageURL) => async (dispatch, getState) => {
    const newCategory = {
        "id": id,
        "name": name,
        "imageURL": imageURL
    };
    await axios.put(`${config.API_URL}/categories/${id}`, newCategory);
    const response = await axios.get(`${config.API_URL}/categories`);
    dispatch({
        type: TYPES.FETCH_ADMIN_CATEGORIES,
        payload: response.data
    });
};

export const removeCategory = (id) => async (dispatch, getState) => {
     await axios.delete(`${config.API_URL}/categories/${id}`);
    const response = await axios.get(`${config.API_URL}/categories`);
    dispatch({
        type: TYPES.FETCH_ADMIN_CATEGORIES,
        payload: response.data
    });
};