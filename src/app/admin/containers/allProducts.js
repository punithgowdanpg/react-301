import {connect} from "react-redux";

import AdminProducts from "../components/allProducts";
import { getAdminProducts, getCategoryProducts, addProduct, updateProduct, removeProduct } from '../state/actions/productActions';
import { getAdminCategory} from '../state/actions/addCategoryActions';

const mapStateToProps = state => {
    return {
        allCategories: state.allCategories,
        allProducts: state.allProducts
      };
};
  
export default connect(mapStateToProps, { getAdminProducts,getCategoryProducts,getAdminCategory, addProduct, updateProduct, removeProduct })(AdminProducts)