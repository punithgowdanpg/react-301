import {connect} from "react-redux";

import AdminCategories from "../components/allCategories";
import { getAdminCategory, addCategory, removeCategory, updateCategory } from '../state/actions/addCategoryActions';

const mapStateToProps = state => {
    return {
        allCategories: state.allCategories
      };
};
  
export default connect(mapStateToProps, { getAdminCategory,addCategory, removeCategory, updateCategory })(AdminCategories)