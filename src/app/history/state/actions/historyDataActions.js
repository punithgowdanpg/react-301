import axios from 'axios';
import { TYPES } from '../../../shared/constants/actionTypes';
import config from '../../../../config';
export const getHistory = (userId) => async (dispatch, getState) => {
    const response = await axios.get(`${config.API_URL}/users/${userId}/sentGifts`);
    dispatch({
        type: TYPES.FETCH_HISTORY,
        payload: response.data
    });
};

