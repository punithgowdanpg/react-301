import axios from 'axios';
import { TYPES } from '../../../shared/constants/actionTypes';
import config from '../../../../config';
export const getReceivedHistory = (userEmail) => async (dispatch, getState) => {
    const response = await axios.get(`${config.API_URL}/receivedGifts?userId=${userEmail}`);
    dispatch({
        type: TYPES.FETCH_RECEIVED_HISTORY,
        payload: response.data
    });
};

