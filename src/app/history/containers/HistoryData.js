import { connect } from "react-redux";
import { getHistory } from '../state/actions/historyDataActions.js';
import HistoryData from "../components/HistoryData";

const mapStateToProps = (state) => ({
    sentHistory: state.sentHistory,
    auth: state.auth
})

export default connect(mapStateToProps, {
    getHistory,
})(HistoryData);