import { connect } from "react-redux";
import { getReceivedHistory } from '../state/actions/receivedDataActions';
import ReceivedGiftsData from "../components/ReceivedGiftsData";

const mapStateToProps = (state) => ({
    receivedHistory: state.receivedHistory,
    auth: state.auth
})

export default connect(mapStateToProps, {
    getReceivedHistory,
})(ReceivedGiftsData);