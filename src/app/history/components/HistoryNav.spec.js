import React from 'react';
import { shallow } from 'enzyme';
import HistoryNav from './HistoryNav';

describe('History Nav component', () => {
    it('should render History Nav correctly', () => {
        const wrapper = shallow(<HistoryNav />);
        expect(true).toMatchSnapshot();
    });
});