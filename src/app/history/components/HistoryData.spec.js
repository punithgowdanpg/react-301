import React from 'react';
import { shallow } from 'enzyme';
import HistoryData from './HistoryData';

describe('History Data component', () => {
    it('should render History Data correctly', () => {
        const wrapper = shallow(<HistoryData />);
        expect(true).toMatchSnapshot();
    });
});