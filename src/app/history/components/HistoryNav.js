import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import NoSsr from '@material-ui/core/NoSsr';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import HistoryData from '../containers/HistoryData';
import ReceivedGiftsData from '../containers/ReceivedGiftsData';

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

function LinkTab(props) {
  return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
appBar:{
  backgroundColor: 'white',
  color: '#202124',
}
});

class HistoryNav extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;

    return (
      <NoSsr>
        <div className={classes.root}>
          <AppBar position="static" className={classes.appBar}>
            <Tabs variant="fullWidth" value={value} onChange={this.handleChange}>
              <LinkTab label="Sent Gifts" href="sentgifts" />
              <LinkTab label="Received Gifts" href="received" />
            </Tabs>
          </AppBar>
         
          {value === 0 && 
          
            <TabContainer >
              
              {/* Display Sent gifts table { /*Have to call sent gifts table data */ } 
                      <HistoryData />
            </TabContainer>}
       
          {value === 1 && 
          <TabContainer>
              
              {/* Display Received gifts table   { /*Have to call received gifts table data */ } 
                <ReceivedGiftsData />
          </TabContainer>}
        </div>
      </NoSsr>
    );
  }
}

HistoryNav.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HistoryNav);
