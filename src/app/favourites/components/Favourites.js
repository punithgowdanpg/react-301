//import React from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import history from '../../shared/utils/history';
import Grid from '@material-ui/core/Grid';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import SubHeader from '../../shared/components/SubHeader/SubHeader'
import Divider from "@material-ui/core/Divider";

const styles = theme => ({
  card: {
    maxWidth: 280,
    maxHeight: 280,
    margin: "auto",
    marginBottom: "20px",
    transition: "0.3s",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)",
      transform: "scale(1.1) rotate(3deg)",
      cursor: "pointer",
    }
  },
  media: {
    paddingTop: "56.25%",
    maxWidth: "200"
  },
  content: {
    textAlign: "left",
    padding: theme.spacing.unit * 3
  },
  divider: {
    margin: "10"
  },
  heading: {
    fontWeight: "bold",
    fontSize: "14px"
  },
  noResult: {
    fontStyle: 'italic',
    textAlign: 'center'
  },
  subheading: {
    lineHeight: 1.8,
    maxHeight: "15px",
    width: "150px",
    display: "inline-block"
  },
  avatar: {
    display: "inline-block",
    border: "2px solid white",
    "&:not(:first-of-type)": {
      marginLeft: -theme.spacing.unit
    }
  }
});

class Favourites extends React.Component {
  componentDidMount() {
    const { userId } = this.props.auth;
    this.props.getFavourites(userId)
  }
  routeToProductDetails = (e) => {
    const productId = e.target.parentElement.getAttribute('data-id');
    history.push(`/products/${productId}/`)
  }

  removeFavourite = (e) => {
    e.stopPropagation();
    const productId = e.target.parentElement.getAttribute('data-id');
    this.props.removeFavorites(productId);
  }

  renderFavList = () => {
    const { classes } = this.props;
    if(this.props.favourites.length === 0){
      return (
        <Grid item xs={12} >
            <Typography variant="h5" className={classes.noResult} gutterBottom>
                No Favourites Added
            </Typography>
        </Grid>
      )
    }
    return this.props.favourites.map((fav) => {
      return (
        <Grid item xs={12} md={3} key={fav.id}>
          <Card className={classes.card} 
            data-id={fav.id}
            onClick={this.routeToProductDetails} >
            <CardMedia
              className={classes.media}
              image={fav.imageUrl}
            />
            <CardContent className={classes.content}>
              <Typography
                className={classes.heading}
                variant={"h6"}
              >
                {fav.productName}
              </Typography>
              <Divider className={classes.divider} light />
              <Typography
                className={classes.subheading}
                variant={"caption"}
              >
                {fav.buyoutPoints} Points
              </Typography>
              <Button onClick={this.removeFavourite} data-id={fav.id} style={{justifyItems: 'right'}} size="small" color="primary">
                  Remove
                </Button>
            </CardContent>
          </Card>
        </Grid>
      )
    });
  }
  render() {
    const { classes } = this.props;
    return (
      <Grid container className={classes.root} spacing={0} alignItems="center" justify="center">
          <Grid item xs={12} >
            <SubHeader heading="Favourites" />
          </Grid>
          {this.renderFavList()}
          <Grid item xs={12} >
          
          </Grid>
      </Grid>
    );
  }
}

Favourites.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Favourites);

