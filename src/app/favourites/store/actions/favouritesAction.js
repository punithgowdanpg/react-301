import axios from 'axios';
import { TYPES } from "../../../shared/constants/actionTypes";
import config from '../../../../config';

export const getFavourites = (userId) => async (dispatch, getState) => {
    const response = await axios.get(`${config.API_URL}/wishlist?userId=${userId}`);
    dispatch({
        type: TYPES.GET_FAVOURITES,
        payload: response.data
    });
};