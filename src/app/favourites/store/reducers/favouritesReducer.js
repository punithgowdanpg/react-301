import { TYPES } from "../../../shared/constants/actionTypes";

const INITIAL_STATE = []

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.GET_FAVOURITES:
            return [...action.payload];
        case TYPES.ADD_FAVOURITES:
            return [...state, action.payload]
        case TYPES.REMOVE_FAVOURITES:
            return state.filter(function( obj ) {
                return obj.id !== action.payload;
            });
        default:
            return state;
    }
};