import { connect } from "react-redux";
import Favourites from "../components/Favourites";
import { getFavourites } from "../store/actions/favouritesAction"; 
import { removeFavorites } from '../../products/state/actions/productDetailAction';

const mapStateToProps = (state) => ({
    favourites: state.favourites,
    auth: state.auth
})

export default connect(mapStateToProps, {
    getFavourites,
    removeFavorites
})(Favourites);