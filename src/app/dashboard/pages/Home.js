import React from 'react';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Carousel from '../containers/Carousel'
import CategoriesGrid from '../../categories/containers/CategoriesGrid';
const styles = theme => ({
    root: {
        flexGrow: 1
    }
});

class Home extends React.Component {
    render() {
        const { classes } = this.props;
        return (
            <Grid className={classes.root} container justify="center">
                <Grid item xs={12} md={12}>
                    <Carousel />
                </Grid>
                <Grid item xs={12} md={12} >
                <Typography variant="h5"> Categories </Typography>
                    <CategoriesGrid />
                </Grid>

            </Grid >
        );
    }
};

export default withStyles(styles)(Home);