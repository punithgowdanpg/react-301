import {connect} from "react-redux";

import Carousel from "../components/Carousel";
import { getCarouselData } from '../state/actions/carouselActions';

const mapStateToProps = state => {
    return {
      carouselData: state.carouselData
    };
};
  
export default connect(mapStateToProps, {
  getCarouselData
})(Carousel)