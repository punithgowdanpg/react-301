// import axios from 'axios';
import { TYPES } from '../../../shared/constants/actionTypes';

export const getCarouselData = (formValues) => async (dispatch, getState) => {
  // const response = await axios.get('https://myrewards7.azurewebsites.net/categories');
  const data = [
    {
      imgPath:
        "https://media.neerus.com/media/wysiwyg/gift-card-banner.jpg"
    },
    {
      imgPath:
        "https://www.adlabsimagica.com/gift-card//files/images/homepagebanner_giftcard_2.jpg"
    },
    {
      imgPath:
        "https://www.supermacs.ie/wp-content/uploads/2018/08/2018_1126-Gift-Cert-Web-Banner1.jpg"
    }
  ];
  dispatch({
      type: TYPES.FETCH_CAROUSEL_DATA,
      payload: data
  });
};
