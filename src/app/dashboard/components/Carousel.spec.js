import React from 'react';
import { shallow } from 'enzyme';
import Carousel from './Carousel';
import renderer from 'react-test-renderer';
import {
    getCarouselData
} from '../state/actions/carouselActions';
describe('Carousel component', () => {
    test('should render Carousel correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<Carousel />);
        expect(wrapper).toMatchSnapshot();
    });
});