import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Fade from '@material-ui/core/Fade';
// import MobileStepper from '@material-ui/core/MobileStepper';
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";


const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "99%",
    margin: "1% auto",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 280,
    [theme.breakpoints.down('md')]: {
      height: 100
    }
  },
  img: {
    height: 280,
    overflow: "hidden",
    width: "98%",
    position: "absolute",
    left: "1%",
    [theme.breakpoints.down('md')]: {
      height: 100
    }
  },
  buttonPrev: {
    position: "absolute",
    left: "0%",
    backgroundColor: "#fff",
    opacity: 0.7,
    zIndex: 99,
    [theme.breakpoints.down('md')]: {
      minWidth: "unset"
    }
  },
  buttonNext: {
    position: "absolute",
    right: "0%",
    backgroundColor: "#fff",
    opacity: 0.7,
    zIndex: 1,
    [theme.breakpoints.down('md')]: {
      minWidth: "unset"
    }
  }
});

class Carousel extends React.Component {
  interval;
  state = {
    activeStep: 0
  };
  componentDidMount() {
    this.props.getCarouselData();
    this.interval = setInterval(() => {
      this.setState(prevState => ({
        activeStep:
          prevState.activeStep === this.props.carouselData.length - 1
            ? 0
            : prevState.activeStep + 1
      }));
    }, 3500);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  resetInterval = () => {
    clearInterval(this.interval);
    this.interval = setInterval(() => {
      this.setState(prevState => ({
        activeStep:
          prevState.activeStep === this.props.carouselData.length - 1
            ? 0
            : prevState.activeStep + 1
      }));
    }, 3500);
  };

  handleNext = () => {
    this.resetInterval();
    this.setState(prevState => ({
      activeStep:
        prevState.activeStep === this.props.carouselData.length - 1
          ? 0
          : prevState.activeStep + 1
    }));
  };

  handleBack = () => {
    this.resetInterval();
    this.setState(prevState => ({
      activeStep:
        prevState.activeStep === 0
          ? this.props.carouselData.length - 1
          : prevState.activeStep - 1
    }));
  };

  render() {
    const { classes, theme } = this.props;
    const { activeStep } = this.state;
    return (
      <div className={classes.root}>
        <Button
          className={classes.buttonPrev}
          size="medium"
          onClick={this.handleBack}
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </Button>
        {this.props.carouselData.map((item, index) => {
            return (
              <Fade
                key={index}
                timeout = {1500}
                in={index === activeStep}
              >
                <img
                  className={classes.img}
                  src={item.imgPath}
                  alt={item.label}
                />
              </Fade>
            );
        })}
        <Button
          size="medium"
          className={classes.buttonNext}
          onClick={this.handleNext}
        >
          {theme.direction === "rtl" ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </Button>
      </div>
    );
  }
}

Carousel.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};


export default withStyles(styles, { withTheme: true })(Carousel);
