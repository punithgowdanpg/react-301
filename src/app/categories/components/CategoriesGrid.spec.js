import React from 'react';
import { shallow } from 'enzyme';
import CategoriesGrid from './CategoriesGrid';
describe('Categories Grid component', () => {
    test('should render Categories grid correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<CategoriesGrid />);
        expect(wrapper).toMatchSnapshot();
    });
});