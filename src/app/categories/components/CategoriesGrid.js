import React, { PureComponent } from 'react'
import CategoryCard from '../../shared/components/CategoryCard/CategoryCard'
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Loader from '../../shared/components/Loader/Loader'
import history from '../../shared/utils/history'
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    category: {
        padding: '10px'
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
});
class CategoriesGrid extends PureComponent {


    componentDidMount() {
        this.props.getCategory();
    }

    renderCategoryGrid(progress) {
        const { classes } = this.props;
        if (this.props.categories.length === 0) {
            return <Loader />
        }
        return this.props.categories.map(category => (
            <Grid item xs={12} md={4} key={category.id} className={classes.category} onClick={() => {
                history.push(`${category.id}/products/`)
            }} >
                <CategoryCard categoryData={category} />
            </Grid>
        ))
    }

    render() {
        const { classes } = this.props;
        return (
            <Grid container justify="center" className={classes.root}>
                {this.renderCategoryGrid(classes.progress)}
            </Grid>
        )
    }
}



export default (withStyles(styles)(CategoriesGrid));