import { connect } from "react-redux";

import CategoriesGrid from "../components/CategoriesGrid";
import {getCategory} from "../state/actions/categoryActions"

const mapStateToProps = (state) => {
    return { categories: state.categories }
};


export default connect(mapStateToProps, {
    getCategory
})(CategoriesGrid);