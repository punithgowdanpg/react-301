import React from 'react'
import CategoriesGrid from '../containers/CategoriesGrid';
import { Typography } from '@material-ui/core';
export default function Categories() {
  return (
    <>
    <Typography variant="h5"> Categories </Typography>
      {/* <SubHeader heading="Categories" /> */}
      <CategoriesGrid />
    </>
  )
}
