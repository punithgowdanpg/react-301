import axios from 'axios';
import { TYPES } from '../../../shared/constants/actionTypes';
import config from '../../../../config';

export const getCategory = (formValues) => async (dispatch, getState) => {

    const response = await axios.get(`${config.API_URL}/categories`);
    dispatch({
        type: TYPES.FETCH_CATEGORIES,
        payload: response.data
    });
};

