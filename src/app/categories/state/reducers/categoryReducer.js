import { TYPES } from "../../../shared/constants/actionTypes";

const INITIAL_STATE = []

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.FETCH_CATEGORIES:
            return [...action.payload];
        case TYPES.CLEAR_CATEGORIES:
            return []
        default:
            return state;
    }
};