import React from 'react'
import Typography from '@material-ui/core/Typography';
export default function SubHeader({ heading, variant }) {
    return (
        <Typography fontWeight="fontWeightRegular" gutterTop gutterBottom variant={variant? variant : 'h2'} component="h1" color="textSecondary" >
            {heading}
        </Typography>
    )
}
