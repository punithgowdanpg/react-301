import React from 'react';
import { shallow } from 'enzyme';
import ProductCard from './ProductCard';

describe('ProductCard component', () => {
    it('should render ProductCard correctly', () => {
        const wrapper = shallow(<ProductCard />);
        expect(true).toMatchSnapshot();
    });
});