import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CardActions from '@material-ui/core/CardActions';
const styles = {
  card: {
    maxWidth: '100%',
  },
  media: {
    objectFit: 'cover',
    maxHeight: '200px',
    minHeight: '200px'
  },
  gridcontainer: {
    alignContent: 'center',
    alignItems: 'center'
  },
  title:{
    minHeight:'60px',
    maxHeight:'60px'
  }
};

const getRating =(productDetails)=> {
  if (productDetails.rating) { return productDetails.rating }
  else {
    return 'Not yet rated!'
  }
}
const getBrand =(productDetails=>{
  return productDetails.brand
})

function ProductCard(props) {
  const { classes, product } = props;
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={product.name}
          className={classes.media}
          image={product.imageUrl}
          title={product.name}
        />
        <CardContent>
          <Typography className={classes.title} gutterBottom variant="h5" component="h2">
            {product.name}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Grid container spacing={24} className={classes.gridcontainer}>
        <Grid item xs={6} md={6}>
            Brand:

            {getBrand(product)}
          </Grid>
          <Grid item xs={6} md={6}>
            Rating:

            {getRating(product)}
          </Grid>
          <Grid item xs={4} md={4}>
            Points: {product.buyoutPoints}
          </Grid>
          {/* <Grid item xs={4} md={4}>
            <IconButton>
              <Icon>
                favorite_border
              </Icon>
            </IconButton>
          </Grid> */}
         
        </Grid>
      </CardActions>
    </Card>
  );
}

ProductCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductCard);
