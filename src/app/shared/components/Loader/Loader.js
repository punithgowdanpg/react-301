import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress';
export default function Loader() {
    return (
        <CircularProgress size={100} thickness={2} />
    )
}
