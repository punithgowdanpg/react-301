import React from 'react';
import { shallow } from 'enzyme';
import Loader from './Loader';
import renderer from 'react-test-renderer';
describe('Loader component', () => {
    it('should render Loader correctly', () => {
        const wrapper = shallow(<Loader />);
        expect(wrapper).toMatchSnapshot();
    });
});



