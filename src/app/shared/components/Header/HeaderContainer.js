import {connect} from "react-redux";

import Header from "./Header";
import { 
    updateSearchTxt, 
    getAllProducts 
} from '../../../products/state/actions/productSearchActions';

const mapStateToProps = (state) => {
    return { auth: state.auth }
};

export default connect(mapStateToProps, {
    updateSearchTxt,
    getAllProducts
})(Header);