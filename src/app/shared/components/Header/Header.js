import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import MoreIcon from '@material-ui/icons/MoreVert';
import { Link } from 'react-router-dom';
import GoogleAuth from '../../../auth/containers/GoogleAuth';
import history from '../../../shared/utils/history';
import RewardPoints from '../../../rewardPoints/containers/RewardPoints';
import Avatar from '@material-ui/core/Avatar';


const styles = theme => ({
  root: {
    width: '100%',
    marginBottom: '70px',
  },
  appbar: {
    background: 'linear-gradient(45deg, #1a1c23 30%, #cc2929 90%)',
    boxShadow: '0 3px 5px 2px rgb(104, 64, 69)'
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    textDecoration: 'none',
    color: '#fff',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  link: {
    textDecoration: 'none !important',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  avatar: {
    position: 'relative',
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
});

class Header extends React.Component {
  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
    searchTxt: '',
    open: false
  };

  handleModalClose = () => {
    this.setState({ open: false });
  };

  handleAddRewardPoints = event => {
    this.setState({ open: true })
    this.handleMenuClose();
  };

  handleProfileMenuOpen = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleProfile = () => {
    history.push(`/profile`);
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };
  handleHistory = () => {
    history.push(`/userhistory/`);
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };
  handleAdmin = () => {
    history.push(`/admin/`);
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleFavourites = () => {
    history.push(`/favourites`);
    this.setState({ anchorEl: null });
    this.handleMobileMenuClose();
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  OnSerchTxtChange = (event) => {
    this.setState({ searchTxt: event.target.value });
  };

  searchProducts = () => {
    history.push(`/products?search=${this.state.searchTxt}`);
    this.props.updateSearchTxt(`${this.state.searchTxt}`);
    this.props.getAllProducts(`${this.state.searchTxt}`);
  }

  render() {
    const { anchorEl, mobileMoreAnchorEl } = this.state;
    const { classes } = this.props;
    const isMenuOpen = Boolean(anchorEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

    const renderMenu = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <MenuItem onClick={this.handleProfile}>Profile</MenuItem>
        <MenuItem onClick={this.handleFavourites}>Favorites</MenuItem>
        <MenuItem onClick={this.handleAddRewardPoints}>My Rewards</MenuItem>
        <MenuItem onClick={this.handleHistory}>History</MenuItem>
        <MenuItem className={classes.link} onClick={this.handleAdmin}>Admin</MenuItem>
      </Menu>
    );

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMenuClose}
        onClick={this.handleProfileMenuOpen}
      >
        <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
        <MenuItem onClick={this.handleFavourites}>Favorites</MenuItem>
        <MenuItem onClick={this.handleAddRewardPoints}>My Rewards</MenuItem>
        <MenuItem onClick={this.handleHistory}>History</MenuItem>
        <MenuItem className={classes.link} onClick={this.handleAdmin}>Admin</MenuItem>
      </Menu>
    );

    return (
      <div className={classes.root}>
        <AppBar position="fixed" className={classes.appbar}>
          <Toolbar>
            {/* <IconButton className={classes.menuButton} color="inherit" aria-label="Open drawer">
              <MenuIcon />
            </IconButton> */}
            <Typography variant="h6" color="inherit" noWrap>
              <Link className={classes.title} to="/">I-Gift</Link>
            </Typography>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                onChange={this.OnSerchTxtChange}
                onKeyPress={(e) => {
                  if (e.key === 'Enter') {
                    this.searchProducts()
                  }
                }}
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
              />
            </div>
            <div className={classes.grow} />
            <div className={classes.sectionDesktop}>
              <GoogleAuth />
              {this.props.auth.isSignedIn && (
                <IconButton
                  aria-owns={isMenuOpen ? 'material-appbar' : undefined}
                  aria-haspopup="true"
                  onClick={this.handleProfileMenuOpen}
                  color="inherit"
                >
            <Avatar 
            alt="User" 
            src={this.props.auth.userImg}
            className={classes.avatar} 
            />
                </IconButton>
              )}
            </div>

            <div className={classes.sectionMobile}>
              <GoogleAuth />
              {this.props.auth.isSignedIn && (
                <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen} color="inherit">
                  <MoreIcon />
                </IconButton>
              )}
            </div>
          </Toolbar>
        </AppBar>
        {renderMenu}
        {renderMobileMenu}
        <RewardPoints
          open={this.state.open}
          onClose={this.handleModalClose}
        />
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);
