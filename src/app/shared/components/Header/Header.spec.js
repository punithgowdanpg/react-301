import React from 'react';
import { shallow } from 'enzyme';
import Header from './Header';
import renderer from 'react-test-renderer';
import {
    updateSearchTxt
} from '../../../products/state/actions/productSearchActions';
import { TYPES } from "../../constants/actionTypes";
describe('Header component', () => {
    test('should render Header correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<Header />);
        expect(wrapper).toMatchSnapshot();
    });
});