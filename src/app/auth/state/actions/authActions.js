import { TYPES } from "../../../shared/constants/actionTypes";
import { addUsers } from './userAction';

export const currentUser = (userId, userEmail, userName, userImg, rewardPoints, isAdmin) => {
    return {
        type: TYPES.SIGN_IN,
        payload: {
            userId,
            userEmail, 
            userName,
            userImg,
            rewardPoints,
            isAdmin
        }
    }
};

export const signOut = () => {
    return {
        type: TYPES.SIGN_OUT
    }
};

export const signIn = (userId, userEmail, userName, userImg) => {
    return (dispatch, getState) => {
        dispatch(addUsers(userId, userEmail, userName, userImg))
    }
};
