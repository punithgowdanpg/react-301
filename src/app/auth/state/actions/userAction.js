import axios from 'axios';
import { TYPES } from "../../../shared/constants/actionTypes";
import { currentUser } from './authActions';
import config from '../../../../config';

export const updateUser = (userId, userEmail, userName) => {
    return {
        type: TYPES.UPDATE_USER,
        payload: {
            userId,
            userEmail, 
            userName
        }
    }
}

export const addUsers = (userId, userEmail, userName, userImg) => async (dispatch, getState) => {
    const userList = await axios.get(`${config.API_URL}/users`);
    let userData = userList.data.find(o => o.id === userId);
    if (!(userList.data.filter(e => e.id === userId).length > 0)) {
        const userData = {
            "id": userId,
            "name": userName,
            "email": userEmail,
            "userImg": userImg,
            "rewardPoints": 50000,
            "isAdmin": "false"
        };
        const response = await axios.post(`${config.API_URL}/users`, userData);
        return response;
    }
    dispatch(currentUser(userId, userEmail, userName, userImg, userData.rewardPoints, userData.isAdmin))
};

export const updateRewards = (userId, payloadObject) => async (dispatch, getState) => {
    const response = await axios.put(`${config.API_URL}/users/${userId}`, payloadObject);
    dispatch({
        type: TYPES.UPDATE_REWARDS,
        payload: response.data
    });
};

export const getUsers = (formValues) => async (dispatch, getState) => {
    const response = await axios.get(`${config.API_URL}/users`);
    dispatch({
        type: TYPES.FETCH_USERS,
        payload: response.data
    });
};