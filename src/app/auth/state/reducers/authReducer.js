import { TYPES } from "../../../shared/constants/actionTypes";


const INITIAL_STATE = {
    isSignedIn: null,
    userId: null,
    userEmail: null,
    userName: null,
    rewardPoints: null,
    isAdmin: "false"
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.SIGN_IN:
            return {
                ...state,
                isSignedIn: true,
                userId: action.payload.userId,
                userEmail: action.payload.userEmail,
                userName: action.payload.userName,
                userImg: action.payload.userImg,
                rewardPoints: action.payload.rewardPoints,
                isAdmin: action.payload.isAdmin,
            };
        case TYPES.UPDATE_USER_REWARDS:
            return { ...state, 
                isSignedIn: true, 
                userId: action.payload.id,
                userEmail: action.payload.userEmail,
                userName: action.payload.userName,
                userImg: action.payload.userImg,
                rewardPoints: action.payload.rewardPoints,
                isAdmin: action.payload.isAdmin };
        case TYPES.SIGN_OUT:
            return { ...state, isSignedIn: false, userId: null, userEmail: null, userName: null, isAdmin: null };
        case TYPES.UPDATE_REWARDS:
            return { ...state, rewardPoints: action.payload.rewardPoints }
        default:
            return state;
    }
};