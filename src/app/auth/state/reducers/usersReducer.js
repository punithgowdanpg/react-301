import { TYPES } from "../../../shared/constants/actionTypes";

const INITIAL_STATE = {
    usersList: []
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case TYPES.FETCH_USERS: 
            return { ...state, usersList: action.payload };
        case TYPES.ADD_USERS:
            return { ...state, usersList: action.payload };
        default:
            return state;
    }
};
