import React from 'react';
import { shallow } from 'enzyme';
import GoogleAuth from './GoogleAuth';

describe('Google Auth component', () => {
    it('should render Google Auth correctly', () => {
        const wrapper = shallow(<GoogleAuth />);
        expect(true).toMatchSnapshot();
    });
});