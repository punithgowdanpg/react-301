import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
    },
    btnTxt: {
        color: '#fff',
        paddingTop:'5px',
        marginLeft: '5px',
    },
    authButton: {
        backgroundColor: 'black',
        verticalAlign: 'bottom',
        margin: "13% 0",
        '&:hover' :{
            backgroundColor: '#f44336',
        }
    },
    googleIcon: {
        color: '#fff',
    },
    button: {
        margin: theme.spacing.unit,
    },
    
});


class GoogleAuth extends React.Component {
    componentDidMount() {
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init({
                clientId: '946608709685-0vqi70emlkf09smvr9mft8gvm5v9jb32.apps.googleusercontent.com',
                scope: 'email'
            }).then(() => {
                this.auth = window.gapi.auth2;
                this.onAuthChange(this.auth.getAuthInstance().isSignedIn.get());
                this.auth.getAuthInstance().isSignedIn.listen(this.onAuthChange);
            });
        });
    };

    onAuthChange = (isSignedIn) => {
        if(isSignedIn) {
            const userId = this.auth.getAuthInstance().currentUser.get().getId();
            const userEmail = this.auth.getAuthInstance().currentUser.get()
                .getBasicProfile().getEmail();
            const userName = this.auth.getAuthInstance().currentUser.get()
                .getBasicProfile().getName();
            const userImg = this.auth.getAuthInstance().currentUser.get()
                .getBasicProfile().getImageUrl();
            this.props.signIn(userId, userEmail, userName, userImg);
        } else {
            this.props.signOut();
        }
    };

    onSignIn = () => {
        this.auth.getAuthInstance().signIn();
    };

    onSignOut = () => {
        this.auth.getAuthInstance().signOut();
    }

    googleIcon = () => {
        return (
            <img src="data:image/svg+xml;base64,
                PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp
                3aWR0aD0iMjQiIGhlaWdodD0iMjQiCnZpZXdCb3g9IjAgMCAxOTIgMTkyIgpzdHlsZT0iIGZpbG
                w6IzAwMDAwMDsiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyIgc3Ryb2tlPSJub
                25lIiBzdHJva2Utd2lkdGg9IjEiIHN0cm9rZS1saW5lY2FwPSJidXR0IiBzdHJva2UtbGluZWpv
                aW49Im1pdGVyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS1kYXNoYXJyYXk9IiIgc3R
                yb2tlLWRhc2hvZmZzZXQ9IjAiIGZvbnQtZmFtaWx5PSJub25lIiBmb250LXdlaWdodD0ibm9uZS
                IgZm9udC1zaXplPSJub25lIiB0ZXh0LWFuY2hvcj0ibm9uZSIgc3R5bGU9Im1peC1ibGVuZC1tb
                2RlOiBub3JtYWwiPjxwYXRoIGQ9Ik0wLDE5MnYtMTkyaDE5MnYxOTJ6IiBmaWxsPSJub25lIj48
                L3BhdGg+PGcgZmlsbD0iI2ZmZmZmZiI+PHBhdGggZD0iTTEwMC4zNiw4MS45MTJ2MzAuNTY4aDQ
                zLjU2Yy01LjY5NiwxOC41MiAtMjEuMTc2LDMxLjc3NiAtNDMuNTYsMzEuNzc2Yy0yNi42NTYsMC
                AtNDguMjY0LC0yMS42MDggLTQ4LjI2NCwtNDguMjU2YzAsLTI2LjY0OCAyMS42MDgsLTQ4LjI1N
                iA0OC4yNjQsLTQ4LjI1NmMxMS45ODQsMCAyMi45MjgsNC4zOTIgMzEuMzY4LDExLjYyNGwyMi41
                MTIsLTIyLjUxMmMtMTQuMjE2LC0xMi45NTIgLTMzLjEyOCwtMjAuODU2IC01My44OCwtMjAuODU
                2Yy00NC4xOTIsMCAtODAuMDE2LDM1LjgxNiAtODAuMDE2LDgwYzAsNDQuMTg0IDM1LjgyNCw4MC
                A4MC4wMTYsODBjNjcuMTY4LDAgODEuOTkyLC02Mi44IDc1LjQwOCwtOTMuOTg0eiI+PC9wYXRoPjwvZz48L2c+PC9zdmc+"
                alt="google icon" />
        )
    };

    renderAuthButton() {
        const { classes } = this.props;
        if(this.props.isSignedIn === null) {
            return null;
        }else if(this.props.isSignedIn) {
            return ( <Button className={classes.authButton} fontWeight="fontWeightLight" variant="contained" onClick={this.onSignOut}>

                    <Typography className={classes.btnTxt}  gutterBottom>
                        Logout
                    </Typography>
                </Button>);
        }else {
            return (<Button className={classes.authButton} fontWeight="fontWeightLight" variant="contained" onClick={this.onSignIn}>
            {this.googleIcon()}
            <Typography className={classes.btnTxt} gutterBottom >
                Login
            </Typography>
            
           </Button>);
        }
    }
    render() {
        return <div>{this.renderAuthButton()}</div>
    }
}


export default (withStyles(styles)(GoogleAuth));