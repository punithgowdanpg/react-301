import { connect } from "react-redux";

import GoogleAuth from "../components/GoogleAuth";
import {signIn, signOut}  from "../state/actions/authActions";
const mapStateToProps = state => {
  return {
    isSignedIn: state.auth.isSignedIn
  };
};

export default connect(
  mapStateToProps,
  { signIn, signOut }
)(GoogleAuth);
