import {connect} from "react-redux";

import AdminDashboard from "../components/adminDashbaord";
import { getAdminCategory } from '../../admin/state/actions/addCategoryActions';
import { getAdminProducts} from '../../admin/state/actions/productActions';
import { getUserCount} from '../state/actions/userActions';
import { getGiftsCount} from '../state/actions/giftActions';

const mapStateToProps = state => {
    return {
        allCategories: state.allCategories,
        allProducts: state.allProducts,
        allUsers: state.allUsers,
        sentGifts: state.sentGifts
      };
};
  
export default connect(mapStateToProps, { getAdminCategory,getAdminProducts, getUserCount, getGiftsCount })(AdminDashboard)