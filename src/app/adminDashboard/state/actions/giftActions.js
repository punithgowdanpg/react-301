import axios from 'axios';
import { TYPES } from "../../../shared/constants/actionTypes";
import config from '../../../../config';
export const getGiftsCount= (formValues) => async (dispatch, getState) => {

    const response = await axios.get(`${config.API_URL}/sentGifts`);
    dispatch({
        type: TYPES.FETCH_GIFTS_COUNT,
        payload: response.data
    });
};