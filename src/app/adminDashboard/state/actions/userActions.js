import axios from 'axios';
import { TYPES } from "../../../shared/constants/actionTypes";
import config from '../../../../config';

export const getUserCount= (formValues) => async (dispatch, getState) => {

    const response = await axios.get(`${config.API_URL}/users`);
    dispatch({
        type: TYPES.FETCH_USER_COUNT,
        payload: response.data
    });
};
