import { TYPES } from "../../../shared/constants/actionTypes";


const INITIAL_STATE = [];

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case TYPES.FETCH_USER_COUNT:
            return [...action.payload];
        default:
            return state;
    }
};