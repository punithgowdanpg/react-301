import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import {Bar} from 'react-chartjs-2';
import Loader from '../../shared/components/Loader/Loader';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    margin: 0,
    padding: "2%"
  },
  title: {
    color: "#fff"
  },
  value: {
    color: "#fff",
    marginTop: "10%",
    [theme.breakpoints.down('md')]: {
      fontSize: "2.5rem"
    },
  },
  graph: {
    width: "100%",
    boxShadow: "3px 3px #d5d5d5",
    border: "1px solid #d5d5d5",
    margin: "4% 2%"
  },
  label: {
    color: "rgba(0, 0, 0, 0.87)",
    textAlign: "center"
  },
  details: {
      width:"100%",
      height: 100,
      borderRadius: 15,
      textAlign:"center",
      padding:"5%",
      color: "#fff !important",
      background:"linear-gradient(-25deg, #1a1c23 30%, #cc2929 90%)",
      [theme.breakpoints.up('sm')]: {
        height: 150,
      },
  }
});

class AdminDashboard extends React.Component {
  state = {
   };
   data = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [
      {
        label: 'My First dataset',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: [65, 59, 80, 81, 56, 55, 40]
      }
    ]
  };
  componentDidMount() {
    this.props.getAdminProducts();  
    this.props.getAdminCategory();  
    this.props.getUserCount();  
    this.props.getGiftsCount();  
  }
  render() {
    const { classes } = this.props;
    // const { open, selected, add, categoryId } = this.state;
    if(this.props.allProducts.length === 0 ){
      return (
        <Grid container alignItems="center"
        justify="center" style={{ height: "90vh" }} className={classes.root} spacing={16}>
        <Loader/>
        </Grid>
      )
    }
    else {
        let accumulatedTotals = this.props.allProducts.reduce(function (acc, curr) {
            if (typeof acc[curr.categoryId] == 'undefined') {
              acc[curr.categoryId] = 1;
            } else {
              acc[curr.categoryId] += 1;
            }
            return acc;
          }, [])
          accumulatedTotals = accumulatedTotals.slice(1)
          let labels = [];
          // eslint-disable-next-line array-callback-return
          this.props.allCategories.map((category)=>{
            labels.push(category.name);
          });
    return (
      <Grid container className={classes.root} spacing={16}>
      <Grid item xs={12} md={12} sm={12} height={200}>
      <Grid container className={classes.demo} justify="center" spacing={32}>
      <Grid key={"categories"} item xs={6} sm={3}>
      <div className={classes.details}>
      <Typography gutterBottom variant="h5" component="h2"  className={classes.title}>Categories</Typography>
      <Typography gutterBottom variant="h3" component="h2" className={classes.value}>{this.props.allCategories.length}</Typography>
      </div>
      </Grid>
      <Grid key={"categories"} item xs={6} sm={3}>
      <div className={classes.details}>
      <Typography gutterBottom variant="h5" component="h2" className={classes.title}>Products</Typography>
      <Typography gutterBottom variant="h3" component="h2" className={classes.value}>{this.props.allProducts.length}</Typography>
      </div>
      </Grid>
      <Grid key={"categories"} item xs={6} sm={3}>
      <div className={classes.details}>
      <Typography gutterBottom variant="h5" component="h2" className={classes.title}>Users</Typography>
      <Typography gutterBottom variant="h3" component="h2" className={classes.value}>{this.props.allUsers.length}</Typography>
      </div>
      </Grid>
      <Grid key={"categories"} item xs={6} sm={3}>
      <div className={classes.details}>
      <Typography gutterBottom variant="h5" component="h2" className={classes.title}>Sent Gifts</Typography>
      <Typography gutterBottom variant="h3" component="h2" className={classes.value}>{this.props.sentGifts.length}</Typography>
      </div>
      </Grid>
      </Grid>
      </Grid>

      <Grid item xs={12} md={12} sm={12} height={200}>
      <Grid container className={classes.demo} justify="center" spacing={32}>
      <Grid key={"categories"} item xs={12} sm={6} className={classes.graph}>
      <Bar
      data={ {
        labels: labels,
        datasets: [
          {
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: accumulatedTotals
          }
        ]
      }}
      width={100}
      height={70}
      options={{
        maintainAspectRatio: true,
        responsive: true,
        legend: false
      }}
    />
    <Typography gutterBottom variant="h6" component="h6"  className={classes.label}>Products By Category</Typography>
      </Grid>
      </Grid>
      </Grid>
      
      </Grid>
    );
      }
}
}

AdminDashboard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};


export default withStyles(styles, { withTheme: true })(AdminDashboard);
