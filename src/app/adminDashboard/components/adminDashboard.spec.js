import React from 'react';
import { shallow } from 'enzyme';
import AdminDashboard from './adminDashbaord';
describe('Admin Dashboard component', () => {
    test('should render dashboard correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<AdminDashboard />);
        expect(wrapper).toMatchSnapshot();
    });
});