import React from 'react';
import { shallow } from 'enzyme';
import RewardPoints from './RewardPoints';
describe('Reward Points component', () => {
    test('should render Reward Points modal correctly', () => {
        jest.mock('@material-ui/core/styles', () => ({
            withStyles: styles => component => component
        }));
        const wrapper = shallow(<RewardPoints />);
        expect(wrapper).toMatchSnapshot();
    });
});