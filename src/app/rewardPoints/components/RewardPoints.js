import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const styles = {
  paper: {
    width: "30%",
    height: 370,
    overflow: "hidden"
  },
  wrapper: {
    margin: "2% 5%"
  },
  subtitle: {
    fontWeight: "500"
  },
  textField: {
    margin: "auto",
    width: "100%"
  },
  button: {
    float: "right",
    marginTop: "6%",
    marginLeft: "5%"
  },
  values: {
    margin: "3% 0"
  },
  valueButton: {
    margin: "2% 2.5%",
    width: "20%"
  },
  title: {
    textAlign: "center"
  },
  slider: {
    width: "90%",
    margin: "5% auto"
  },
  sliderBar: {
    margin: "2% 0"
  },
  thumb: {
    cursor: "pointer",
    borderColor: "#333",
    borderStyle: "solid",
    height: "11px",
    top: "1px",
    width: "12px",
    backgroundColor: "rgba(100, 100, 100, 1)",
    borderRadius: "50%",
    position: "absolute"
  },
  output: {
    pointerEvents: "none",
    margin: 0,
    width: 50,
    height: 50,
    left: "-20px",
    lineHeight: 30,
    borderRadius: "50% 50% 0 50%",
    backgroundColor: "#4ac4ac",
    textAlign: "center",
    transform: "rotate(225deg)",
    position: "absolute",
    top: 20
    /*left:45px*/
  },
  topUp: {
    fontSize: 14
  }
};

class RewardPoints extends React.Component {
  state = {
    value: 0
  };

  handleClose = () => {
    this.props.onClose();
  };

  handleAdd = (e, addValue) => {
    this.setState(() => ({
      value: addValue
    }));
  };

  topUp = () => {
    this.props.updateUser(
      parseInt(this.props.auth.rewardPoints) + parseInt(this.state.value)
    );
    this.setState(() => ({
      value: 0
    }));
  };

  render() {
    const { classes, onClose, auth, updateUser, ...other } = this.props;
    // const { value } = this.state;
    return (
      <Dialog
        onClose={this.handleClose}
        classes={{ paper: classes.paper }}
        aria-labelledby="simple-dialog-title"
        {...other}
      >
        <DialogTitle id="simple-dialog-title" classes={{ root: classes.title }}>
          My Reward Points
        </DialogTitle>
        <div className={classes.wrapper}>
          <Typography
            align="left"
            className={classes.subtitle}
            variant="subtitle1"
          >
            Available Balance{" "}
            <span style={{ float: "right" }}>{auth.rewardPoints}</span>
          </Typography>
          <hr />
          <br />
          <Typography align="left" className={classes.topUp}>
            Top-Up your Balance
          </Typography>
          <div className={classes.values}>
            <Button
              variant="contained"
              color="primary"
              className={classes.valueButton}
              onClick={e => this.handleAdd(e, 500)}
            >
              500
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.valueButton}
              onClick={e => this.handleAdd(e, 1000)}
            >
              1000
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.valueButton}
              onClick={e => this.handleAdd(e, 1500)}
            >
              1500
            </Button>
            <Button
              variant="contained"
              color="primary"
              className={classes.valueButton}
              onClick={e => this.handleAdd(e, 2000)}
            >
              2000
            </Button>
          </div>
          <TextField
            id="standard-number"
            label="Enter a Value"
            value={this.state.value}
            onChange={e => this.handleAdd(e, e.target.value)}
            type="number"
            className={classes.textField}
            InputLabelProps={{
              shrink: true
            }}
            margin="normal"
          />

          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this.handleClose}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={this.topUp}
          >
            Top Up
          </Button>
        </div>
      </Dialog>
    );
  }
}

RewardPoints.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool,
  onClose: PropTypes.func
};

export default withStyles(styles)(RewardPoints);
