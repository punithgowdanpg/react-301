import axios from 'axios';
import { TYPES } from "../../../shared/constants/actionTypes";
import config from '../../../../config';
export const updateUser = (rewardPoints) => async (dispatch, getState) => {
    let currentUser = getState().auth;
    const userData = {
        "name": currentUser.userName,
        "email": currentUser.userEmail,
        "rewardPoints": rewardPoints,
        "isAdmin": currentUser.isAdmin,
    };
    const response = await axios.put(`${config.API_URL}/users/${currentUser.userId}`, userData);
    dispatch({
        type: TYPES.UPDATE_USER_REWARDS,
        payload: response.data
    });
};