import { connect } from "react-redux";
import RewardPoints from "../components/RewardPoints";
import { updateUser } from "../state/actions/rewardActions"
const mapStateToProps = state => {
  return {
    auth: state.auth,
  };
};

export default connect(
  mapStateToProps, { updateUser }
)(RewardPoints);
