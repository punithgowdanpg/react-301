import React from "react";
import { Router, Route } from 'react-router-dom';
import ProductSearch from './app/products/pages/ProductSearch';
import Header from './app/shared/components/Header/HeaderContainer';
import history from './app/shared/utils/history';
import AdminCategories from './app/admin/containers/allCategories';
import AdminProducts from './app/admin/containers/allProducts';
import AdminDashboard from './app/adminDashboard/containers/adminDashboard';

//Lazy load modules, split bigger modules into smaller
import Loadable from 'react-loadable';
import Loader from './app/shared/components/Loader/Loader'
function Loading() {
    return (
        <Loader  />
    )
}

const LoadableHomeComponent = Loadable({
    loader: () => import('./app/dashboard/pages/Home'),
    loading: Loading,
});

const LoadableProductsComponent = Loadable({
    loader: () => import('./app/products/pages/Products'),
    loading: Loading,
  });

const LoadableProfileComponent = Loadable({
    loader: () => import('./app/profile/containers/Profile'),
    loading: Loading,
  });
  
const LoadableCategoriesComponent = Loadable({
    loader: () => import('./app/categories/pages/Categories'),
    loading: Loading,
  });
  
const LoadableProductDetailComponent = Loadable({
    loader: () => import('./app/products/containers/ProductDetail'),
    loading: Loading,
  });

const LoadableFavouritesComponent = Loadable({
    loader: () => import('./app/favourites/containers/Favourites'),
    loading: Loading,
  });

const LoadableHistoryNavComponent = Loadable({
    loader: () => import('./app/history/components/HistoryNav'),
    loading: Loading,
  });

const LoadableAdminComponent = Loadable({
    loader: () => import('./app/admin/pages/adminHome'),
    loading: Loading,
  });

export default function Routes(props) {
    return (
        <Router history={history}>
        <Header />
        <React.Fragment>
          <Route path="/" exact component={LoadableHomeComponent} />
          <Route path="/categories" exact component={LoadableCategoriesComponent} />
          <Route path="/:categoryId/products" component={LoadableProductsComponent} />
          <Route path="/products" exact component={ProductSearch} />
          <Route path="/products/:productId" component={LoadableProductDetailComponent} />
          <Route exact path='/admin/categories' component={()=> (
            <LoadableAdminComponent>
            <AdminCategories/>
            </LoadableAdminComponent>  
          )} />
          <Route exact path='/admin/allProducts' component={()=> (
            <LoadableAdminComponent>
            <AdminProducts/>
            </LoadableAdminComponent>  
          )} />
          <Route exact path='/admin/' component={()=> (
            <LoadableAdminComponent>
            <AdminDashboard/>
            </LoadableAdminComponent>  
          )} />
          <Route path="/favourites" component ={LoadableFavouritesComponent} />
          <Route path="/userhistory" component ={LoadableHistoryNavComponent} />
          <Route path="/profile" component ={LoadableProfileComponent} />
        </React.Fragment>
      </Router>
    )
}
