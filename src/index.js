import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import * as serviceWorker from './serviceWorker';
import reduxThunk from 'redux-thunk';
import invariant from 'redux-immutable-state-invariant';

import App from './App';
import combineReducers from './combinedReducers';
// import * as actionCreators from './actions'; 
import { SnackbarProvider } from 'notistack';

const composeEnhancers = composeWithDevTools({ trace: true, traceLimit: 25 });
const store = createStore(combineReducers, {}, composeEnhancers(
    applyMiddleware(invariant(), reduxThunk)
));

ReactDOM.render(
    <Provider store={store}>
        <SnackbarProvider maxSnack={3}>
        <App />
        </SnackbarProvider>
    </Provider>,
    document.querySelector('#root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
