import { combineReducers } from 'redux';
import authReducer from './app/auth/state/reducers/authReducer';
import categoryReducer from './app/categories/state/reducers/categoryReducer';
import usersReducer from './app/auth/state/reducers/usersReducer';
import productReducer from './app/products/state/reducers/productReducer';
import productSearchReducer from './app/products/state/reducers/productSearchReducer';
import carouselReducer from './app/dashboard/state/reducers/carouselReducer';
import productDetailReducer from './app/products/state/reducers/productDetailReducer'
import historyDataReducer from './app/history/state/reducers/historyDataReducer';
import receivedDataReducer from './app/history/state/reducers/receivedDataReducer';
import addCategoryReducer from './app/admin/state/reducers/addCategoryReducers';
import favouritesReducer from './app/favourites/store/reducers/favouritesReducer';
import adminProductReducer from './app/admin/state/reducers/productReducers';
import userReducers from './app/adminDashboard/state/reducers/userReducers';
import giftReducers from './app/adminDashboard/state/reducers/giftReducers';

export default combineReducers({
    auth: authReducer,
    users: usersReducer,
    categories: categoryReducer,
    products: productReducer,
    productSearch: productSearchReducer,
    carouselData: carouselReducer,
    productDetail: productDetailReducer,
    sentHistory: historyDataReducer,
    receivedHistory: receivedDataReducer,
    allCategories:addCategoryReducer,
    favourites: favouritesReducer,
    allProducts:adminProductReducer,
    allUsers:userReducers,
    sentGifts:giftReducers

});